<?php

require_once "modeles/M_tournois.php";
require_once "modeles/M_jeux.php";
require_once "modeles/M_historique.php";
require_once "modeles/M_participer.php";
require_once "modeles/M_membre.php";

class C_mesTournois {

    private $data;
    private $modeleTournois;
    private $modeleJeux;
    private $modeleHistorique;
    private $modeleParticiper;
    private $modeleMembre;

    public function __construct() {
        $this->data = array();
        $this->modeleTournois = new M_tournois();
        $this->modeleJeux = new M_jeux();
        $this->modeleHistorique = new M_historique();
        $this->modeleParticiper = new M_participer();
        $this->modeleMembre = new M_membre();
    }

    public function action_leMessage($color, $titre, $message, $link) {
        require_once "controleurs/C_lemessage.php";
        $controleur = new C_lemessage;
        $controleur -> action_enregisterLeMessage($color, $titre, $message, $link);
    }

    public function action_afficher() {
        if (isset($_SESSION["connecté"]) && isset($_SESSION["membre"]) && $_SESSION["connecté"] == true) {

            $tempData = $this->modeleTournois->GetListe();
            sort($tempData);
            $this->data['lesTournois'] = array();
            $this->data['lesJeux'] = array();
            $this->data['TournoisEquipe'] = array();
            $this->data['TournoisMembre'] = array();
            $this->data['HistoriqueEquipe'] = array();
            $this->data['HistoriqueMembre'] = array();

            foreach($tempData as $tournois) {
                $jeu = $this->modeleJeux->GetJeuById($tournois->GetIdJeu());
                if (!in_array($jeu, $this->data['lesJeux'])) {
                    $this->data['lesJeux'][$jeu->GetId()] = $jeu;
                }
                $tournoi = $this->modeleTournois->GetTournoisById($tournois->GetId());
                if (!in_array($tournoi, $this->data['lesTournois'])) {
                    $this->data['lesTournois'][$tournoi->GetId()] = $tournoi;
                }
            }
            sort($this->data['lesJeux']);
            sort($this->data['lesTournois']);

            $this->data["leMembre"] = $this->modeleMembre->GetMembreById($_SESSION["membreId"]);
            $this->data["TournoisEquipe"] = $this->modeleParticiper->GetParticiperListeByEquipeID($this->data["leMembre"]->GetIdEquipe());
            $this->data["TournoisMembre"] = $this->modeleParticiper->GetParticiperListeByMembreID($this->data["leMembre"]->GetId());
            $this->data["HistoriqueEquipe"] = $this->modeleHistorique->GetHistoriqueListeByEquipeID($this->data["leMembre"]->GetIdEquipe());
            $this->data["HistoriqueMembre"] = $this->modeleHistorique->GetHistoriqueListeByMembreID($this->data["leMembre"]->GetId());

            include_once("vues/V_mesTournois.php");
        } else {
			include_once("vues/V_error404.php");
        }
    }
    
}
