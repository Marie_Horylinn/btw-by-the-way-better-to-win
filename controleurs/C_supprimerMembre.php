<?php

require_once "modeles/M_membre.php";

class C_supprimerMembre {
    private $data;
    private $modeleMembre;

    public function __construct() {
        $this->data = array();
        $this -> modeleMembre = new M_membre();
    }

    public function action_afficher() {
        require_once "vues/v_supprimerJeux.php";
    }

    /*
    * Fonction nécessaire pour ajouter un message en bas à droite, elle ne changera jamais.
    * La couleur prend red, blue ou green
    * Titre Message et lien leur noms sont compréhensibles !
    * ça sert à Enregistrer le message, dans le $_SESSION.
    * C'est tout en bas du index.php qu'on l'affiche !
    */
    public function action_leMessage($color, $titre, $message, $link) {
        require_once "controleurs/C_lemessage.php";
        $controleur = new C_lemessage;
        $controleur -> action_enregisterLeMessage($color, $titre, $message, $link);
    }

    //Le ?int $id, ça veut dire que on peut mettre Null, mais que ça sera tjr un int ! Par exemple.
    public function action_supprimer(?int $id) {
        if (is_null($id)) {
            require_once "vues/V_error404.php";
        } else {
            //On récupère le jeu avant sa suppression pour vérifier qu'il existe bien.
            $oldMembre = $this->modeleMembre->GetMembreById($id);
            if (!is_null($oldMembre)) {
                $membre = $this->modeleMembre->SupprimerMembre($id);
                if (!is_null($membre)) {
                    $this -> action_leMessage("red", "Attention !", "La suppression du membre '" . $oldMembre->GetPseudo() ."' a échoué.", "index.php?page=admin");
                } else {
                    $this -> action_leMessage("green", "Membre supprimé !", "Le membre '" . $oldMembre->GetPseudo() . "' a été supprimé.", "index.php?page=admin");
                }
            } else {
                $this -> action_leMessage("red", "Membre introuvable !", "Le membre avec l'Id '" .$id. "' n'existe pas.\nImpossible de le supprimer.", "index.php?page=admin");
            }
        }
    }
}
