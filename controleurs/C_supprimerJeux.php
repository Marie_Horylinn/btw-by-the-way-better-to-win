<?php

require_once "modeles/M_jeux.php";

class C_supprimerJeux {
    private $data;
    private $modeleJeux;

    public function __construct() {
        $this->data = array();
        $this -> modeleJeux = new M_jeux();
    }

    public function action_afficher() {
        require_once "vues/v_supprimerJeux.php";
    }

    /*
    * Fonction nécessaire pour ajouter un message en bas à droite, elle ne changera jamais.
    * La couleur prend red, blue ou green
    * Titre Message et lien leur noms sont compréhensibles !
    * ça sert à Enregistrer le message, dans le $_SESSION.
    * C'est tout en bas du index.php qu'on l'affiche !
    */
    public function action_leMessage($color, $titre, $message, $link) {
        require_once "controleurs/C_lemessage.php";
        $controleur = new C_lemessage;
        $controleur -> action_enregisterLeMessage($color, $titre, $message, $link);
    }

    //Le ?int $id, ça veut dire que on peut mettre Null, mais que ça sera tjr un int ! Par exemple.
    public function action_supprimer(?int $id) {
        if (is_null($id)) {
            require_once "vues/V_error404.php";
        } else {
            //On récupère le jeu avant sa suppression pour vérifier qu'il existe bien.
            $oldJeu = $this->modeleJeux->GetJeuById($id);
            if (!is_null($oldJeu)) {
                $jeu = $this->modeleJeux->SupprimerJeu($id);
                if (!is_null($jeu)) {
                    $this -> action_leMessage("red", "Attention !", "La suppession du jeu '" . $oldJeu->GetNom() ."' a échoué.", "index.php?page=accueil#nosjeux");
                } else {
                    $this -> action_leMessage("green", "Jeu supprimé !", "Le jeu '" . $oldJeu->GetNom() . "' a été supprimé.", "index.php?page=accueil#nosjeux");
                }
            } else {
                $this -> action_leMessage("red", "Jeu introuvable !", "Le jeu avec l'Id '" .$id. "' n'existe pas.\nImpossible de le supprimer.", "index.php?page=accueil#nosjeux");
            }
        }
    }
}
