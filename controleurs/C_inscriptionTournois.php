<?php

require_once "modeles/M_tournois.php";
require_once "modeles/M_participer.php";
require_once "modeles/M_membre.php";

class C_inscriptionTournois {

    private $data;
    private $modeleTournois;
    private $modeleParticiper;
    private $modeleMembre;

    public function __construct() {
        $this->data = array();
        $this->modeleTournois = new M_tournois();
        $this->modeleParticiper = new M_participer();
        $this->modeleMembre = new M_membre();
    }

    public function action_leMessage($color, $titre, $message, $link) {
        require_once "controleurs/C_lemessage.php";
        $controleur = new C_lemessage;
        $controleur -> action_enregisterLeMessage($color, $titre, $message, $link);
    }

    //Le ?int $id, ça veut dire que on peut mettre Null, mais que ça sera tjr un int ! Par exemple.
    public function action_inscriptionTournois(?int $idTournois) {

        if (is_null($idTournois)) {
            require_once "vues/V_error404.php";
        } else {
            if (isset($_SESSION["membreId"])) {
                $membre = $this->modeleMembre->GetMembreById($_SESSION["membreId"]);
                $this->data["lesMembres"] = array();
                $members = $this->modeleMembre->GetMembreByEquipeId($membre->GetIdEquipe());
                $participation = null;
                $tournois = $this->modeleTournois->GetTournoisById($idTournois);
                if (!is_null($tournois)) {
                    if ($tournois->GetNbEquipe() > $this->modeleParticiper->GetCountEquipesParticipantsByTournoi($idTournois)) {
                        foreach($members as $mb) {
                            if (!is_null($mb)) {
                                $participation = $this->modeleParticiper->AddParticipation($mb->GetIdEquipe(), $idTournois, $mb->GetId());
                            }
                        }
                        if (!is_null($participation))
                            $this->action_leMessage("green", "Inscription réussie", "Vous avez validé l'inscription de votre équipe au tournoi " .$idTournois, "index.php?page=listetournois");
                        else
                            $this->action_leMessage("red", "Inscription échouée", "L'inscription de votre équipe au tournoi a échouée.", "index.php?page=listetournois");
                    } else {
                        $this->action_leMessage("red", "Inscription échouée", "Le nombre maximum d'équipes pour ce tournoi a été atteint.", "index.php?page=listetournois");
                    }
                } else {
                    $this->action_leMessage("red", "Inscription échouée", "L'inscription de votre équipe au tournoi a échouée. Le tournois est introuvable.", "index.php?page=listetournois");
                }
                
            } else {
                $this->action_leMessage("red", "Inscription échouée", "L'inscription de votre équipe au tournoi a échouée. Le membre est introuvable.", "index.php?page=listetournois");
            }
        }

        
    }
    
}
