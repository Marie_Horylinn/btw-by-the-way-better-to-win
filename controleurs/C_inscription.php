<?php
require_once("modeles/M_membre.php");
require_once("modeles/M_equipe.php");

class C_inscription {
    private $data;
    private $modeleMembre;
    private $modeleEquipe;

    public function __construct() {
        $this->data = array();
        $this -> modeleMembre = new M_membre();
        $this->modeleEquipe=new M_equipe();
    }

    public function action_saisie() {
        $this -> controleurMenu -> FillData($this->data);
        require_once "vues/v_saisieEmploye.php";
    }
    
    public function action_leMessage($color, $titre, $message, $link) {
        require_once "controleurs/C_lemessage.php";
        $controleur = new C_lemessage;
        $controleur -> action_enregisterLeMessage($color, $titre, $message, $link);
    }

    public function action_ajout($nom, $prenom, $mail,$tel,$pseudo,$mdp,$image,$idequipe, $idgroupe) {
      $id=$this->modeleMembre->nbmembre();
        if (is_null($this->modeleMembre->GetMembre($nom))) {
            $membre = $this->modeleMembre->AddMembre($id, $nom, $prenom, $mail,$tel,$pseudo,$mdp,$image,$idequipe, $idgroupe);
            if (is_null($membre)) {
                $this->action_leMessage("red", "Inscription échouée", "L'inscription a échouée pour une raison indéterminée.", "index.php?page=inscription");
            } else {
                $this->action_leMessage("green", "Inscription réussie", "Vous pouvez à présent vous connecter, ".$membre->GetPseudo(), "index.php?page=connexion");
            }
        } else {
            $this->action_leMessage("red", "Inscription échouée", "Le pseudo ".$pseudo." existe déjà, inscription annulée.", "index.php?page=inscription");
        }
    }

    public function action_ajoutGerant($nom, $prenom, $mail,$tel,$pseudo,$mdp,$image,$idequipe, $idgroupe, $nomEquipe, $descEquipe, $imageEquipe) {
        $idEquipe = $this->modeleEquipe->nbEquipe();
        if (is_null($this->modeleEquipe->GetEquipeByNom($nomEquipe))) {

            $im = imagecreatefromjpeg($imageEquipe['tmp_name']);
            imagejpeg($im, 'assets/img/equipes/'.$idEquipe.$imageEquipe['name'], 90);
            $imageEquipe = $idEquipe.$imageEquipe['name'];
            $equipe = $this->modeleEquipe->AjouterEquipe($nomEquipe, $descEquipe, $imageEquipe);
            if (is_null($equipe)) {
                $this->action_leMessage("red", "Inscription échouée", "L'inscription n'a pas pu être effectuée car l'équipe n'a pas pu être crée.", "index.php?page=inscription");
            } else {
                $id=$this->modeleMembre->nbmembre();
                  if (is_null($this->modeleMembre->GetMembre($pseudo))) {
                    $membre = $this->modeleMembre->AddMembre($id, $nom, $prenom, $mail,$tel,$pseudo,$mdp,$image,$equipe->GetId(), $idgroupe);
                      if (is_null($membre)) {
                        $this->action_leMessage("red", "Inscription échouée", "L'inscription a échouée pour une raison indéterminée.", "index.php?page=inscription");
                      } else {
                        $this->action_leMessage("green", "Inscription réussie", "Vous pouvez à présent vous connecter, ".$membre->GetPseudo(), "index.php?page=connexion");

                      }
                  } else {
                    $this->action_leMessage("red", "Inscription échouée", "Le pseudo ".$pseudo." existe déjà, inscription annulée.", "index.php?page=inscription");
                  }
            }
        } else {
            $this->action_leMessage("red", "Inscription échouée", "L'inscription n'a pas pu être effectuée une équipe existe déjà avec ce nom.", "index.php?page=inscription");
        }
      }

    public function action_afficher()
    {
        if (isset($_SESSION["connecté"]) && $_SESSION["connecté"] == true) {
            require_once("vues/V_error404.php");
        } else {
            $this->data['lesEquipes']=$this->modeleEquipe->GetEquipesListe();
            $this->date['nbEquipe']=$this->modeleEquipe->nbequipe();
            require_once("vues/V_inscription.php");
        }

    }
}
