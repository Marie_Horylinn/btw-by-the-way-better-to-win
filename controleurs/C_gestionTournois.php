<?php

require_once "modeles/M_tournois.php";
require_once "modeles/M_jeux.php";

class C_gestionTournois {

    private $data;
    private $modeleTournois;
    private $modeleJeux;

    public function __construct() {
        $this->data = array();
        $this->modeleTournois = new M_tournois();
        $this->modeleJeux = new M_jeux();
    }

    public function action_leMessage($color, $titre, $message, $link) {
        require_once "controleurs/C_lemessage.php";
        $controleur = new C_lemessage;
        $controleur -> action_enregisterLeMessage($color, $titre, $message, $link);
    }

    public function action_creerTournois($nom, $idJeu, $nbParticipant, $dateHeure) {
        $jeu = $this->modeleJeux->GetJeuById($idJeu);
        if (!is_null($jeu)) {
            $tournoi = $this->modeleTournois->AjouterTournois($nom, $dateHeure, $nbParticipant, $idJeu);
            if (!is_null($tournoi)) {
                $this->action_leMessage("green", "Tournoi créé !", "Vous avez créé le tournoi '".$nom."' du jeu ".$jeu->GetNom(), "index.php?page=listetournois");
            } else {
                $this->action_leMessage("red", "Erreur !", "Le tournoi n'a pas pu être créé.", "index.php?page=listetournois");
            }
        } else {
            $this->action_leMessage("red", "Erreur !", "Le tournoi n'a pas pu être créé. Le jeu est introuvable. ID:".$idJeu, "index.php?page=listetournois");
        }
    }

    public function action_supprimerTournois($id) {
        $tournois = $this->modeleTournois->GetTournoisById($id);
        if (!is_null($tournois)) {
            $supp = $this->modeleTournois->SupprimerTournois($id);
            if (is_null($supp)) {
                $this->action_leMessage("green", "Tournoi supprimé !", "Vous avez supprimé le tournoi '".$id."'", "index.php?page=listetournois");
            } else {
                $this->action_leMessage("red", "Erreur !", "Le tournoi n'a pas pu être supprimé.", "index.php?page=listetournois");
            }
        } else {
            $this->action_leMessage("red", "Erreur !", "Le tournoi n'a pas pu être supprimé. Le tournoi est introuvable. ID:".$id, "index.php?page=listetournois");
        }
    }
    
}
