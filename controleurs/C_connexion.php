<?php
require_once("modeles/M_membre.php");
require_once("modeles/M_equipe.php");

class C_connexion
{
  private $data;
  private $modeleMembre;

  public function __construct()
  {
    $this->data = array();
    $this->modeleMembre = new M_membre();
    $this->modeleEquipe = new M_equipe();
  }

  public function action_saisie()
  {
    if (isset($_SESSION["connecté"]) && $_SESSION["connecté"] == true) {
      require_once "vues/V_error404.php";
    } else {
      require_once "vues/V_connexion.php";
    }
  }

  public function action_leMessage($color, $titre, $message, $link)
  {
    require_once "controleurs/C_lemessage.php";
    $controleur = new C_lemessage;
    $controleur->action_enregisterLeMessage($color, $titre, $message, $link);
  }

  public function action_connexion($pseudo, $mdp)
  {
    $unMembre = $this->modeleMembre->GetMembre($pseudo);
    if (is_null($unMembre)) {
      $this->action_leMessage("red", "Pseudo incorrect !", "Le pseudonyme '" . $pseudo . "' n'est pas reconnu.", "index.php?page=connexion");
    } else {
      if ($unMembre->GetMdp() == md5($mdp)) {
        /*Instatiation des différentes variables, membre et MembreId sont nécessaire car parfois c'est géré par le pseudo...*/
        $_SESSION["connecté"] = true;
        $_SESSION["membre"] = $unMembre->GetPseudo();
        $_SESSION["groupe"] = $unMembre->GetIdGroupe(); //Admin 1, Gérant 2, Membre 3
        if ($_SESSION["groupe"] == 1)
          $_SESSION["isAdmin"] = true;
        else
          $_SESSION["isAdmin"] = false;
        $_SESSION["membreId"] = $unMembre->GetId();
        $this->action_leMessage("green", "Connexion établie !", "Bonjour, " . $pseudo, "index.php?page=profil");
      } else {
        $this->action_leMessage("red", "Informations incorrectes !", "Les informations de connexion sont incorrectes.", "index.php?page=connexion");
      }
    }
  }

  public function action_deconnexion()
  {
    if (!isset($_SESSION["membreId"])) {
      include_once "vues/V_error404.php";
    } else {
      unset($_SESSION["connecté"]);
      unset($_SESSION["membre"]);
      unset($_SESSION["groupe"]);
      unset($_SESSION["isAdmin"]);
      unset($_SESSION["membreId"]);
      $this->action_leMessage("green", "Déconnexion réussie !", "A bientôt",  "index.php?page=connexion");
    }
  }
}
