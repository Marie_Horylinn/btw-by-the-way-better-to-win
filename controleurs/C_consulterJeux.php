<?php

require_once "modeles/M_jeux.php";

class C_consulterJeux {

    private $data;
    private $modeleJeux;

    public function __construct() {
        $this->data = array();
        $this->modeleJeux = new M_jeux();
    }

    public function action_listeJeux() {
        $this->data['lesJeux'] = $this->modeleJeux->GetListe();
        require_once "vues/V_listeJeux.php";
    }

    //Le ?int $id, ça veut dire que on peut mettre Null, mais que ça sera tjr un int ! Par exemple.
    public function action_afficherJeu(?int $id, ?string $acronym) {

        if (is_null($id) && is_null($acronym)) {
            require_once "vues/V_error404.php";
        } else {
            if (is_null($id)) {
                $this->data['leJeu'] = $this->modeleJeux->GetJeuByAcronym($acronym);
            } else {
                $this->data['leJeu'] = $this->modeleJeux->GetJeuById($id);
            }
            require_once "vues/V_jeux.php";
        }

        
    }
    
}
