<?php

require_once "modeles/M_jeux.php";

class C_ajouterJeux
{
    private $data;
    private $modeleJeux;

    public function __construct()
    {
        $this->data = array();
        $this->modeleJeux = new M_jeux();
    }

    public function action_saisie()
    {
        require_once "vues/v_saisieJeux.php";
    }

    /*
    * Fonction nécessaire pour ajouter un message en bas à droite, elle ne changera jamais.
    * La couleur prend red, blue ou green
    * Titre Message et lien leur noms sont compréhensibles !
    * ça sert à Enregistrer le message, dans le $_SESSION.
    * C'est tout en bas du index.php qu'on l'affiche !
    */
    public function action_leMessage($color, $titre, $message, $link)
    {
        require_once "controleurs/C_lemessage.php";
        $controleur = new C_lemessage;
        $controleur->action_enregisterLeMessage($color, $titre, $message, $link);
    }

    public function action_ajout($nom, $acronyme, $petite_desc, $description, $image, $nbparticipants)
    {
        $id = $this->modeleJeux->GetNbJeux() + 1; //On récupère le nombre de données dans la bdd, +1 pour avoir l'id de la nouvelle
        //Si on ne trouve pas le jeu (s'il n'existe pas) alors on le crée.
        if (is_null($this->modeleJeux->GetJeuByNom($nom)) && is_null($this->modeleJeux->GetJeuByAcronym($acronyme))) {
            //On  récupère l'image de notre formulaire, pour la créer dans le dossier.
            $im = imagecreatefromjpeg($image['tmp_name']);
            imagejpeg($im, 'assets/img/games/' . $id . $image['name'], 90);
            $image = $id . $image['name'];
            //M_jeux -> AjouterJeu
            $jeu = $this->modeleJeux->AjouterJeu($nom, $acronyme, $petite_desc, $description, $image, $nbparticipants);
            if (is_null($jeu)) {
                //Utilisation de la fonction action_leMessage
                $this->action_leMessage("red", "Attention !", "L'ajout du jeu '" . $nom . "' a échoué pour une raison indéterminée.", "index.php?page=accueil#nosjeux");
            } else {
                $this->action_leMessage("green", "Félicitations !", "Le jeu '" . $jeu->GetNom() . "' a été ajouté.", "index.php?page=accueil#nosjeux");
            }
        } else {
            $this->action_leMessage("red", "Erreur !", "Le jeu '" . $nom . "' existe déjà, \nl'ajout a été annulé.", "index.php?page=accueil#nosjeux");
        }
    }
}
