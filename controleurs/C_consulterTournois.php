<?php

require_once "modeles/M_tournois.php";
require_once "modeles/M_jeux.php";
require_once "modeles/M_participer.php";
require_once "modeles/M_membre.php";

class C_consulterTournois {

    private $data;
    private $modeleTournois;
    private $modeleJeux;
    private $modeleParticipation;
    private $modeleMembre;

    public function __construct() {
        $this->data = array();
        $this->modeleTournois = new M_tournois();
        $this->modeleJeux = new M_jeux();
        $this->modeleParticipation = new M_participer();
        $this->modeleMembre = new M_membre();
    }

    public function action_listeTournois() {
        $tempData = $this->modeleTournois->GetListe();
        sort($tempData);
        $this->data['lesTournois'] = array();
        $this->data['lesJeux'] = array();
        $this->data['lesJeuxEnregister'] = array();
        $this->data['lesParticipations'] = array();
        $this->data['lesJeuxEnregister'] = $this->modeleJeux->GetListe();

        foreach($tempData as $tournois) {
            $jeu = $this->modeleJeux->GetJeuById($tournois->GetIdJeu());
            if (!in_array($jeu, $this->data['lesJeux'])) {
                $this->data['lesJeux'][$jeu->GetId()] = $jeu;
            }
            $tournoi = $this->modeleTournois->GetTournoisById($tournois->GetId());
            if (!in_array($tournoi, $this->data['lesTournois'])) {
                $this->data['lesTournois'][$tournoi->GetId()] = $tournoi;
            }
            if (isset($_SESSION["membreId"])) {
                $membre = $this->modeleMembre->GetMembreById($_SESSION["membreId"]);
                $part = $this->modeleParticipation->GetParticipationByEquipeAndTournoi($membre->GetIdEquipe(), $tournois->GetId());
                if (!is_null($part))
                    $this->data['lesParticipations'][] = $part->GetIdTournois();
            }
        }
        sort($this->data['lesJeux']);
        sort($this->data['lesTournois']);

        require_once "vues/V_listeTournois.php";
    }

    //Le ?int $id, ça veut dire que on peut mettre Null, mais que ça sera tjr un int ! Par exemple.
    public function action_afficherTournois(?int $id) {

        if (is_null($id)) {
            require_once "vues/V_error404.php";
        } else {
            if (!is_null($id)) {
                $this->data['leTournois'] = $this->modeleTournois->GetTournoisById($id);
            }
            require_once "vues/V_tournois.php";
        }

        
    }
    
}
