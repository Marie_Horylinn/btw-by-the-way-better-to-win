<?php

require_once "modeles/M_jeux.php";

class C_accueil
{

    private $data;
    private $modeleJeux;

    public function __construct()
    {
        $this->data = array();
        $this->modeleJeux = new M_jeux();
    }

    //Afficher les jeux dans la page d'accueil
    public function action_afficherAccueil()
    {
        $this->data['lesJeux'] = $this->modeleJeux->GetListe();
        require_once "vues/V_accueil.php";
    }
}
