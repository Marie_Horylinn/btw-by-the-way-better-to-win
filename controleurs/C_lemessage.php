<?php


class C_lemessage {

    public function __construct() {

    }

    /*
    * Quand on enregistre le message, on va d'abord définir la couleur, et la mettre en $_SESSION (pour l'avoir partout !)
    * C'est différent du data, car on l'a partout sans recharger les fichiers.
    *
    * Puis on charge dans le $_SESSION notre lien, titre, message, et le timer actuel de quand le message a été enregistré !
    * Timer nécessaire pour éviter d'avoir le message plusieurs fois.
    * Puis on écho un scrit javascript, pour pouvoir retourner sur Index.php avec notre lien $link, sans perdre notre $_SESSION
    */
    public function action_enregisterLeMessage(?string $color, $titre, $message, $link) {
        if (is_null($color))
            $_SESSION["colorleMessage"] = "blue.png";
        else {
            switch ($color) {
                case "red":
                    $_SESSION["colorleMessage"] = "red.png";
                    break;
                case "green":
                    $_SESSION["colorleMessage"] = "green.jpg";
                    break;
                default:
                    $_SESSION["colorleMessage"] = "blue.png";
                    break;
            }
        }
        $_SESSION["lienleMessage"] = $link;
        $_SESSION["titreleMessage"] = $titre;
        $_SESSION["leMessage"] = $message;
        $_SESSION["lemessagelife"] = time();
        echo "<script>var path = window.location.href;var i = path.lastIndexOf('/') + 1;var loc = path.substring(0, i ).concat('".$link."');window.location.assign(loc);</script>";
    }

    /*
    * On vérifie que nos sessions soient mises, et donc on affiche le message.
    */
    public function action_afficherLeMessage() {
        if (isset($_SESSION["leMessage"])) {
            require_once "vues/V_lemessage.php";
        }
    } 
    
}
