<?php

require_once "modeles/M_tournois.php";
require_once "modeles/M_historique.php";
require_once "modeles/M_equipe.php";
require_once "modeles/M_participer.php";

class C_resultatTournois {

    private $data;
    private $modeleTournois;
    private $modeleHistorique;
    private $modeleEquipe;
    private $modeleParticiper; //Début de création pour pouvoir enregistrer les scores autrement qu'en BDD....

    public function __construct() {
        $this->data = array();
        $this->modeleTournois = new M_tournois();
        $this->modeleEquipe = new M_equipe();
        $this->modeleHistorique = new M_historique();
        $this->modeleParticiper = new M_participer();
    }

    //Le ?int $id, ça veut dire que on peut mettre Null, mais que ça sera tjr un int ! Par exemple.
    public function action_afficherResultatTournois(?int $id) {

        if (is_null($id)) {
            require_once "vues/V_error404.php";
        } else {
            if (!is_null($id)) {
                $this->data['lesEquipes'] = array();
                $this->data['leHistorique'] = array();
                $this->data['leTournois'] = $this->modeleTournois->GetTournoisById($id);
                $this->data['leHistorique'] = $this->modeleHistorique->GetClassement($id);
                if (empty($this->data["leHistorique"])) {
                    $this->data["laParticipation"] = $this->modeleParticiper->GetParticiperListeByTournoiID($id);
                    $this->data["lesEquipesParticipantes"] = $this->modeleParticiper->GetEquipesParticipantsByTournoi($id);
                    //On random 3 fois la liste, pour générer les niveaux
                    shuffle($this->data["lesEquipesParticipantes"]);  
                    shuffle($this->data["lesEquipesParticipantes"]);
                    shuffle($this->data["lesEquipesParticipantes"]);
                    $i = 1;
                    foreach($this->data["lesEquipesParticipantes"] as $lesEquipes) {
                        foreach($this->data["laParticipation"] as $laParticipation) {
                            if ($lesEquipes == $laParticipation->GetIdEquipe()) {
                                $this->modeleHistorique->AddHistorique($laParticipation->GetIdEquipe(), $laParticipation->GetIdTournois(), $laParticipation->GetIdMembre(), $i);
                            }
                        }
                        $i++;
                    }
                }
                $this->data['leHistorique'] = $this->modeleHistorique->GetClassement($id);
                if (!empty($this->data['leHistorique'])) {
                    foreach($this->data['leHistorique'] as $histo) {
                        $this->data['lesEquipes'][] = $this->modeleEquipe->GetEquipeById($histo->GetIdEquipe());
                    }
                }
                require_once "vues/V_resultatTournois.php";
            }
        }

        
    }
    
}
