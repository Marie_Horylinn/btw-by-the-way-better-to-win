<?php

require_once "modeles/M_membre.php";
require_once "modeles/M_groupe.php";
require_once "modeles/M_equipe.php";

class C_admin
{

    private $data;
    private $modeleMembre;

    //Servent dans la vue. V_admin.php
    private $modeleGroupe;
    private $modeleEquipe;

    public function __construct()
    {
        $this->data = array();
        $this->modeleMembre = new M_membre();
        $this->modeleGroupe = new M_groupe();
        $this->modeleEquipe = new M_equipe();
    }

    public function action_afficher()
    {
        $this->data['lesMembres'] = $this->modeleMembre->GetListe();
        require_once "vues/V_admin.php";
    }
}
