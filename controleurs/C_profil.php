<?php
require_once("modeles/M_membre.php");
require_once("modeles/M_equipe.php");
require_once("modeles/M_groupe.php");
require_once("modeles/M_equipe.php");

class C_profil {
    private $data;
    private $modeleMembre;
    private $modeleGroupe;
    private $modeleEquipe;

    public function __construct() {
        $this->data = array();
        $this ->modeleMembre = new M_membre();
        $this->modeleGroupe=new M_groupe();
        $this->modeleEquipe=new M_equipe();
    }

    public function action_leMessage($color, $titre, $message, $link) {
      require_once "controleurs/C_lemessage.php";
      $controleur = new C_lemessage;
      $controleur -> action_enregisterLeMessage($color, $titre, $message, $link);
    }

    public function action_recup(){
      if(isset($_SESSION['membreId'])){
        $pseudo=$_SESSION["membreId"];
        $this->data['leMembre']=$this->modeleMembre->GetMembreById($pseudo);
        $x=$this->data['leMembre']->GetIdGroupe();
        $y=$this->data['leMembre']->GetIdEquipe();

        $this->data['leGroupe']=$this->modeleGroupe->GetGroupeById($x);
        $this->data['lesEquipes'] = $this->modeleEquipe->GetEquipesListe();

        $this->data['lequipe']=$this->modeleEquipe->GetEquipeById($y);
        include_once("vues/V_profil.php");
      } else {
        include_once("vues/V_error404.php");
      }
    }

    public function action_modif($id,$nom, $prenom, $mail,$tel,$pseudo,$mdp,$image,?int $idequipe){
      $oldMembre = $this->modeleMembre->GetMembreById($id);
      if ($oldMembre->GetImage() != "profilbase.png")
        unlink("assets/img/profil/".$oldMembre->GetImage());
      $im = imagecreatefromjpeg($image['tmp_name']);
      imagejpeg($im, 'assets/img/profil/'.$id.$image['name'], 90);
      $image = $id.$image['name'];
      $membre = $this->modeleMembre->modif_membre($id, $nom, $prenom, $mail,$tel,$pseudo,$mdp,$image,$idequipe);
      $this->action_leMessage("green", "Succès", "Modification du profil réussie", "index.php?page=profil");

    }
}
