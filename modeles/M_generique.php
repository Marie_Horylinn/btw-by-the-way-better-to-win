<?php

class M_generique {

    private $cnx;

    public function GetCnx() {
        return $this->cnx;
    }

    /*
    * Attention à ne pas avoir deux "connexion()" en même temps, sans avoir déconnecté la précédente !
    */
    public function connexion() {
        $this -> cnx = mysqli_connect("localhost", "root", "", "btw");
        mysqli_set_charset($this->cnx, "utf8");
    }

    public function deconnexion() {
        mysqli_close($this->cnx);
    }

    /*
    * Quand on supprime une valeur dans une table, et qu'elle est autoincrément,
    * Le prochain insert sera incrémenté comme si aucune valeur n'a été supprimée.
    * Mettre cette fonction quand on supprime, empêche ça
    */
    public function alterAutoincrement($table) {
        $this->connexion();
        $req = "select COALESCE(MAX(id), 1) as cpt FROM ".$table;
        $res = mysqli_query($this->GetCnx(), $req);
        $ligne = mysqli_fetch_assoc($res);
        if ($ligne) {
            $resultat = $ligne['cpt']+1;
        } else {
            $resultat = null;
        }
        $this->deconnexion();
        if (!is_null($resultat)) {
            $this->connexion();
            $req = "alter table ".$table." AUTO_INCREMENT = ".$resultat;
            $res = mysqli_query($this->GetCnx(), $req);
        }
    }


}
