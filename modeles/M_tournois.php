<?php

    require_once "M_generique.php";
    require_once "metiers/Tournois.php";

    class M_tournois extends M_generique {

        public function GetListe() {
            $resultat = array();
            $this -> connexion();
            $req = "select * from tournois";
            $res = mysqli_query($this -> GetCnx(), $req);
            $ligne = mysqli_fetch_assoc($res);

            while ($ligne) {
                $tournois = new Tournois($ligne["id"], $ligne["nom"], $ligne["DateHeure"], $ligne["nbEquipe"], $ligne["idJeu"]);
                $resultat[] = $tournois;
                $ligne = mysqli_fetch_assoc($res);
            }
            $this->deconnexion();
            return $resultat;
        }

        //Récupérer le tournois par son ID
        public function GetTournoisById($id) {
            $this->connexion();
            $id = mysqli_real_escape_string($this->GetCnx(), $id);
            $req = "select * from tournois where id=".$id.";";
            $res = mysqli_query($this->GetCnx(),$req);
            $ligne = mysqli_fetch_assoc($res);
            if ($ligne) {
                $resultat = new Tournois($ligne["id"], $ligne["nom"], $ligne["DateHeure"], $ligne["nbEquipe"], $ligne["idJeu"]);
            } else {
                $resultat = null;
            }
            $this->deconnexion();
            return $resultat;
        }

        //Récupérer du nombre de tournois
        public function GetNbTournois() {
            $this->connexion();
            $req = "select COALESCE(MAX(id), 1) as cpt from tournois";
            $res = mysqli_query($this->GetCnx(),$req);
            $ligne = mysqli_fetch_assoc($res);
            if ($ligne) {
                $resultat = $ligne["cpt"];
            } else {
                $resultat = null;
            }
            $this->deconnexion();
            return $resultat;
        }

        public function AjouterTournois($nom, $DateHeure, $nbEquipe, $idJeu) {
            //On cherche à avoir le nouvel ID, avant le connexion pour ne pas le faire pendant un autre connexion.
            $nbTournois = $this->GetNbTournois()+1;
            $this->connexion();
            //mysqli_real_escape_string nécessaire pour éviter les erreurs si on rentre n'importe quoi.
            $nom = mysqli_real_escape_string($this->GetCnx(), $nom);
            $DateHeure = mysqli_real_escape_string($this->GetCnx(), $DateHeure);
            $nbEquipe = mysqli_real_escape_string($this->GetCnx(), $nbEquipe);
            $IdJeu = mysqli_real_escape_string($this->GetCnx(), $idJeu);
            $tournoi = new Tournois($nbTournois, $nom, $DateHeure, $nbEquipe, $IdJeu);
            $req = "insert into tournois (nom, DateHeure, nbEquipe, IdJeu) values ('".$nom."','".$DateHeure."',".$nbEquipe.",".$idJeu.")";
            $ok = mysqli_query($this->GetCnx(), $req);
            if(!$ok) {
                $tournoi = null;
            }
            $this -> deconnexion();
            //Si ça a fonctionné, on renvoi le tournois.
            return $tournoi;
        }

        public function SupprimerTournois($id) {
            $game = $this->GetTournoisById($id);
            $this->connexion();
            $id = mysqli_real_escape_string($this->GetCnx(), $id);
            $req = "delete from tournois WHERE id=".$id;
            $tournoi = $id;
            $ok = mysqli_query($this->GetCnx(), $req);
            if ($ok) {
                $tournoi = null;
            }
            $this -> deconnexion();
            $this->alterAutoincrement("tournois");
            return $tournoi;
        }

    }
