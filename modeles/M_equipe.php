<?php
require_once "M_generique.php";
require_once "metiers/Equipe.php";

class M_equipe extends M_generique {

    public function GetEquipesListe() {
        $resultat = array();
        $this->connexion();
        $req = "select distinct id, nom,description from equipe";
        $res = mysqli_query($this->GetCnx(), $req);
        $ligne = mysqli_fetch_assoc($res);

        while ($ligne) {
            $equipe = new Equipe($ligne["id"], $ligne["nom"], $ligne["description"]);
            $resultat[] = $equipe;
            $ligne = mysqli_fetch_assoc($res);
        }
        $this->deconnexion();
        return $resultat;
    }

    public function GetEquipeById($id) {
        $this->connexion();
        $id = mysqli_real_escape_string($this->GetCnx(), $id);
        $req = "select * from equipe where id = ".$id;
        $res = mysqli_query($this->GetCnx(), $req);
        $ligne = mysqli_fetch_assoc($res);
        if ($ligne) {
            $resultat = new Equipe($ligne['id'], $ligne['nom'], $ligne['description']);
        } else {
            $resultat = null;
        }
        $this->deconnexion();
        return $resultat;
    }

    public function GetEquipeByNom($nom) {
        $this->connexion();
        $nom = mysqli_real_escape_string($this->GetCnx(), $nom);
        $req = "select * from equipe where nom = '".$nom."'";
        $res = mysqli_query($this->GetCnx(), $req);
        $ligne = mysqli_fetch_assoc($res);
        if ($ligne) {
            $resultat = new Equipe($ligne['id'], $ligne['nom'],$ligne['description']);
        } else {
            $resultat = null;
        }
        $this->deconnexion();
        return $resultat;
    }

    public function AjouterEquipe($nom, $description, $image) {
        //On cherche à avoir le nouvel ID, avant le connexion pour ne pas le faire pendant un autre connexion.
        $nbEquipe = $this->nbequipe()+1;
        $this->connexion();
        //mysqli_real_escape_string nécessaire pour éviter les erreurs si on rentre n'importe quoi.
        $nom = mysqli_real_escape_string($this->GetCnx(), $nom);
        $description = mysqli_real_escape_string($this->GetCnx(), $description);
        $image = mysqli_real_escape_string($this->GetCnx(), $image);
        $equipe = new Equipe($nbEquipe, $nom, $description, $image);
        $req = "insert into equipe (nom, description, image) values ('".$nom."','".$description."','".$image."')";
        $ok = mysqli_query($this->GetCnx(), $req);
        if(!$ok) {
            $equipe = null;
        }
        $this -> deconnexion();
        //Si ça a fonctionné, on renvoi l'équipe.
        return $equipe;
    }

    public function nbequipe(){
      $this->connexion();
      $req = "select COALESCE(MAX(id), 1) as compteur from equipe";
      $res = mysqli_query($this->GetCnx(), $req);
      $ligne = mysqli_fetch_assoc($res);
      if ($ligne) {
          $resultat = $ligne["compteur"];
      } else {
          $resultat = null;
      }
      $this->deconnexion();
      $this->alterAutoincrement("equipe");
      return $resultat;
    }

}
