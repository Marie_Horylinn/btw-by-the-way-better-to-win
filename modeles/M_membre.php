<?php

    require_once "M_generique.php";
    require_once "metiers/Membre.php";

    class M_membre extends M_generique {

        public function GetListe() {
            $resultat = array();
            $this->connexion();
            $req = "select * from membre";
            $res = mysqli_query($this->GetCnx(), $req);
            $ligne = mysqli_fetch_assoc($res);

            while ($ligne) {
                $membre = new Membre($ligne['id'], $ligne['nom'], $ligne['prenom'], $ligne['mail'], $ligne['tel'],
                                    $ligne['pseudo'], $ligne['mdp'], $ligne['image'], $ligne['id_equipe'], $ligne['id_categorie']);
                $resultat[] = $membre;
                $ligne = mysqli_fetch_assoc($res);
            }
            $this->deconnexion();
            return $resultat;
        }

        public function GetMembre($unpseudo) {
            $this->connexion();
            $unpseudo = mysqli_real_escape_string($this->GetCnx(), $unpseudo);
            $req = "select * from membre where pseudo='".$unpseudo."'";
            $res = mysqli_query($this->GetCnx(), $req);
            $ligne = mysqli_fetch_assoc($res);
            if ($ligne) {
                $membre = new Membre($ligne['id'], $ligne['nom'], $ligne['prenom'], $ligne['mail'], $ligne['tel'],
                                    $ligne['pseudo'], $ligne['mdp'], $ligne['image'], $ligne['id_equipe'], $ligne['id_categorie']);
                $resultat = $membre;
            } else {
                $resultat = null;
            }
            $this->deconnexion();
            return $resultat;
        }

        public function GetMembreByEquipeId($id) {
            $this->connexion();
            $id = mysqli_real_escape_string($this->GetCnx(), $id);
            $resultat = array();
            $req = "select * from membre where id_equipe=".$id;
            $res = mysqli_query($this->GetCnx(), $req);
            $ligne = mysqli_fetch_assoc($res);
            while ($ligne) {
                $membre = new Membre($ligne['id'], $ligne['nom'], $ligne['prenom'], $ligne['mail'], $ligne['tel'],
                                    $ligne['pseudo'], $ligne['mdp'], $ligne['image'], $ligne['id_equipe'], $ligne['id_categorie']);
                $resultat[] = $membre;
                $ligne = mysqli_fetch_assoc($res);
            }
            $this->deconnexion();
            return $resultat;
        }

        public function GetMembreById($id) {
            $this->connexion();
            $id = mysqli_real_escape_string($this->GetCnx(), $id);
            $req = "select * from membre where id=".$id;
            $res = mysqli_query($this->GetCnx(), $req);
            $ligne = mysqli_fetch_assoc($res);
            if ($ligne) {
                $membre = new Membre($ligne['id'], $ligne['nom'], $ligne['prenom'], $ligne['mail'], $ligne['tel'],
                                    $ligne['pseudo'], $ligne['mdp'], $ligne['image'], $ligne['id_equipe'], $ligne['id_categorie']);
                $resultat = $membre;
            } else {
                $resultat = null;
            }
            $this->deconnexion();
            return $resultat;
        }

        public function AddMembre($id,$unnom, $unprenom,$unmail,$untel,$unpseudo,$unmdp,$uneimage,$idequipe,$idgroupe)
        {
          $this->connexion();
          $id = mysqli_real_escape_string($this->GetCnx(), $id);
          $unnom = mysqli_real_escape_string($this->GetCnx(), $unnom);
          $unprenom = mysqli_real_escape_string($this->GetCnx(), $unprenom);
          $unmail = mysqli_real_escape_string($this->GetCnx(), $unmail);
          $untel = mysqli_real_escape_string($this->GetCnx(), $untel);
          $unpseudo = mysqli_real_escape_string($this->GetCnx(), $unpseudo);
          $unmdp = mysqli_real_escape_string($this->GetCnx(), $unmdp);
          $uneimage = mysqli_real_escape_string($this->GetCnx(), $uneimage);
          $idequipe = mysqli_real_escape_string($this->GetCnx(), $idequipe);
          $idgroupe = mysqli_real_escape_string($this->GetCnx(), $idgroupe);
          $membre = null;
          $requete = mysqli_query($this->GetCnx(), "SELECT * FROM membre WHERE pseudo = '".$unpseudo."' AND mail='".$unmail."'") ;

          if(mysqli_num_rows ($requete) >= 1)
          {
            echo "Ce compte existe déjà";
          }
          else
          {
            $unmdp=md5($unmdp);
            $membre = new Membre($id, $unnom, $unprenom, $unmail, $untel, $unpseudo, $unmdp, $uneimage, $idequipe, $idgroupe);
            $inscri="Insert into membre(nom, prenom, mail, tel, pseudo, mdp, image,id_equipe,id_categorie) VALUES ('".$unnom."','".$unprenom."','".$unmail."','".$untel."','".$unpseudo."','".$unmdp."','".$uneimage."',".$idequipe.",".$idgroupe.");";
            mysqli_query($this->GetCnx(),$inscri);
        }
        $this->deconnexion();
        return $membre;
    }

    public function modif_membre($id,$unnom, $unprenom,$unmail,$untel,$unpseudo,$unmdp,$uneimage,?int $uneEquipe){
        $this->connexion();
        $id = mysqli_real_escape_string($this->GetCnx(), $id);
        $unmdp = md5($unmdp);
          $unnom = mysqli_real_escape_string($this->GetCnx(), $unnom);
          $unprenom = mysqli_real_escape_string($this->GetCnx(), $unprenom);
          $unmail = mysqli_real_escape_string($this->GetCnx(), $unmail);
          $untel = mysqli_real_escape_string($this->GetCnx(), $untel);
          $unpseudo = mysqli_real_escape_string($this->GetCnx(), $unpseudo);
          $unmdp = mysqli_real_escape_string($this->GetCnx(), $unmdp);
          $uneimage = mysqli_real_escape_string($this->GetCnx(), $uneimage);
        if (!is_null($uneEquipe)) {
            $uneEquipe = mysqli_real_escape_string($this->GetCnx(), $uneEquipe);
            $modif="update membre set nom='".$unnom."' , prenom='".$unprenom."' ,mail='".$unmail."' , tel='".$untel."' , pseudo='".$unpseudo."' , mdp='".$unmdp."' , image='".$uneimage."' , id_equipe=".$uneEquipe." WHERE id=".$id.";";
        } else {
            $modif="update membre set nom='".$unnom."' , prenom='".$unprenom."' ,mail='".$unmail."' , tel='".$untel."' , pseudo='".$unpseudo."' , mdp='".$unmdp."' , image='".$uneimage."' WHERE id=".$id.";";
        }
      mysqli_query($this->GetCnx(),$modif);
      $this->deconnexion();
    }

    public function nbmembre(){
      $this->connexion();
      $req = "select COALESCE(MAX(id), 1) as compteur from membre";
      $res = mysqli_query($this->GetCnx(), $req);
      $ligne = mysqli_fetch_assoc($res);
      if ($ligne) {
          $resultat = $ligne["compteur"];
      } else {
          $resultat = null;
      }
      $this->deconnexion();
      $this->alterAutoincrement("membre");
      return $resultat+1;
    }

    public function supprimerMembre($idMembre) {
        $oldmembre = $this->GetMembreById($idMembre);
        $membre = $this->GetMembreById($idMembre);
        $this->connexion();
        $idMembre = mysqli_real_escape_string($this->GetCnx(), $idMembre);
        $req = "DELETE FROM participer WHERE id_membre=".$idMembre;
        $res = mysqli_query($this->GetCnx(), $req);
        if ($res) {
            $req = "DELETE FROM historiquetournois WHERE id_membre=".$idMembre;
            $res = mysqli_query($this->GetCnx(), $req);
            if ($res) {
                $req = "DELETE FROM membre WHERE id=".$idMembre;
                $res = mysqli_query($this->GetCnx(), $req);
                if ($res) {
                    $membre = null;
                }
            }
        }
        $this->deconnexion();
        $this->alterAutoincrement("membre");
        if (is_null($membre) && $oldmembre->GetImage() != "profilbase.png") {
            unlink("assets/img/profil/".$oldmembre->GetImage());
        }
        return $membre;
    }
  }
