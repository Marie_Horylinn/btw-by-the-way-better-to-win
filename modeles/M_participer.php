<?php

    require_once "M_generique.php";
    require_once "metiers/Participer.php";

    class M_participer extends M_generique {

        public function GetParticiperListe() {
            $resultat = array();
            $this->connexion();
            $req = "select id_equipe, id_tournois from participer";
            $res = mysqli_query($this->GetCnx(), $req);
            $ligne = mysqli_fetch_assoc($res);

            while ($ligne) {
                $groupe = new Participer($ligne["id_equipe"], $ligne["id_tournois"], $ligne["id_membre"]);
                $resultat[] = $groupe;
                $ligne = mysqli_fetch_assoc($res);
            }
            $this->deconnexion();
            return $resultat;
        }

        public function GetParticipationByEquipeAndTournoi($idEquipe, $IdTournoi) {
            $resultat = null;
            $this->connexion();
            $idEquipe = mysqli_real_escape_string($this->GetCnx(), $idEquipe);
            $IdTournoi = mysqli_real_escape_string($this->GetCnx(), $IdTournoi);
            $req = "select id_equipe, id_tournois, id_membre from participer where id_equipe=".$idEquipe." and id_tournois=".$IdTournoi;
            $res = mysqli_query($this->GetCnx(), $req);
            $ligne = mysqli_fetch_assoc($res);
            if ($ligne) {
                $groupe = new Participer($ligne["id_equipe"], $ligne["id_tournois"], $ligne["id_membre"]);
                $resultat = $groupe;
                $ligne = mysqli_fetch_assoc($res);
            }
            $this->deconnexion();
            return $resultat;
        }

        public function GetParticiperListeByEquipeID($idEquipe) {
            $totalId = array();
            $resultat = array();
            $this->connexion();
            $idEquipe = mysqli_real_escape_string($this->GetCnx(), $idEquipe);
            $req = "select distinct id_tournois, id_equipe, id_membre from participer where id_equipe=".$idEquipe;
            $res = mysqli_query($this->GetCnx(), $req);
            $ligne = mysqli_fetch_assoc($res);

            while ($ligne) {
                if (!in_array($ligne["id_tournois"], $totalId)) {
                    $totalId[] = $ligne["id_tournois"];
                    $groupe = new Participer($ligne["id_equipe"], $ligne["id_tournois"], $ligne["id_membre"]);
                    $resultat[] = $groupe;
                }
                $ligne = mysqli_fetch_assoc($res);
            }
            $this->deconnexion();
            return $resultat;
        }

        public function GetParticiperListeByMembreID($idMembre) {
            $resultat = array();
            $this->connexion();
            $idMembre = mysqli_real_escape_string($this->GetCnx(), $idMembre);
            $req = "select id_equipe, id_tournois, id_membre from participer where id_membre=".$idMembre;
            $res = mysqli_query($this->GetCnx(), $req);
            $ligne = mysqli_fetch_assoc($res);

            while ($ligne) {
                $groupe = new Participer($ligne["id_equipe"], $ligne["id_tournois"], $ligne["id_membre"]);
                $resultat[] = $groupe;
                $ligne = mysqli_fetch_assoc($res);
            }
            $this->deconnexion();
            return $resultat;
        }

        public function GetParticiperListeByTournoiID($idTournoi) {
            $resultat = array();
            $this->connexion();
            $idTournoi = mysqli_real_escape_string($this->GetCnx(), $idTournoi);
            $req = "select id_equipe, id_tournois, id_membre from participer where id_tournois=".$idTournoi;
            $res = mysqli_query($this->GetCnx(), $req);
            $ligne = mysqli_fetch_assoc($res);

            while ($ligne) {
                $groupe = new Participer($ligne["id_equipe"], $ligne["id_tournois"], $ligne["id_membre"]);
                $resultat[] = $groupe;
                $ligne = mysqli_fetch_assoc($res);
            }
            $this->deconnexion();
            return $resultat;
        }

        public function GetEquipesParticipantsByTournoi($idTournoi) {
            $resultat = array();
            $this->connexion();
            $idTournoi = mysqli_real_escape_string($this->GetCnx(), $idTournoi);
            $req = "select distinct id_equipe from participer where id_tournois=".$idTournoi;
            $res = mysqli_query($this->GetCnx(), $req);
            $ligne = mysqli_fetch_assoc($res);
            while ($ligne) {
                $groupe = $ligne["id_equipe"];
                $resultat[] = $groupe;
                $ligne = mysqli_fetch_assoc($res);
            }
            $this->deconnexion();
            return $resultat;
        }

        public function GetCountEquipesParticipantsByTournoi($idTournoi) {
            $resultat = array();
            $this->connexion();
            $idTournoi = mysqli_real_escape_string($this->GetCnx(), $idTournoi);
            $req = "select distinct id_equipe from participer where id_tournois=".$idTournoi;
            $res = mysqli_query($this->GetCnx(), $req);
            $ligne = mysqli_fetch_assoc($res);
            while ($ligne) {
                $groupe = $ligne["id_equipe"];
                $resultat[] = $groupe;
                $ligne = mysqli_fetch_assoc($res);
            }
            $this->deconnexion();
            return count($resultat);
        }

        public function AddParticipation($id_equipe, $id_tournois, $id_membre) {
            $this->connexion();
            $id_equipe = mysqli_real_escape_string($this->GetCnx(), $id_equipe);
            $id_tournois = mysqli_real_escape_string($this->GetCnx(), $id_tournois);
            $id_membre = mysqli_real_escape_string($this->GetCnx(), $id_membre);
            $participer = new Participer($id_equipe, $id_tournois, $id_membre);
            $req = "insert into participer (id_equipe, id_tournois, id_membre) values (".$id_equipe.","."$id_tournois".",".$id_membre.")";
            $ok = mysqli_query($this->GetCnx(), $req);
            if(!$ok) {
                $participer = null;
            }
            $this -> deconnexion();
            //Si ça a fonctionné, on renvoi la participation.
            return $participer;
        }
    }
