<?php

    require_once "M_generique.php";
    require_once "metiers/Jeux.php";

    class M_jeux extends M_generique {

        public function GetListe() {
            $resultat = array();
            $this -> connexion();
            $req = "select * from jeux";
            $res = mysqli_query($this -> GetCnx(), $req);
            $ligne = mysqli_fetch_assoc($res);

            while ($ligne) {
                $jeu = new Jeux($ligne["id"], $ligne["nom"], $ligne["acronyme"], $ligne["petite_desc"], $ligne["description"], $ligne["image"], $ligne["nb_participants"]);
                $resultat[] = $jeu;
                $ligne = mysqli_fetch_assoc($res);
            }
            $this->deconnexion();
            return $resultat;
        }

        //Récupérer le jeu par son ID
        public function GetJeuById($id) {
            $this->connexion();
            $id = mysqli_real_escape_string($this->GetCnx(), $id);
            $req = "select * from jeux where id=".$id."";
            $res = mysqli_query($this->GetCnx(),$req);
            $ligne = mysqli_fetch_assoc($res);
            if ($ligne) {
                $resultat = new Jeux($ligne["id"], $ligne["nom"], $ligne["acronyme"], $ligne["petite_desc"], $ligne["description"], $ligne["image"], $ligne["nb_participants"]);
            } else {
                $resultat = null;
            }
            $this->deconnexion();
            return $resultat;
        }

        //Récupérer le jeu par son acronyme
        public function GetJeuByAcronym($acronym) {
            $this->connexion();
            $acronym = mysqli_real_escape_string($this->GetCnx(), $acronym);
            $req = "select * from jeux where acronyme='".$acronym."' LIMIT 1";
            $res = mysqli_query($this->GetCnx(),$req);
            $ligne = mysqli_fetch_assoc($res);
            if ($ligne) {
                $resultat = new Jeux($ligne["id"], $ligne["nom"], $ligne["acronyme"], $ligne["petite_desc"], $ligne["description"], $ligne["image"], $ligne["nb_participants"]);
            } else {
                $resultat = null;
            }
            $this->deconnexion();
            return $resultat;
        }

        //Récupérer le jeu par son nom
        public function GetJeuByNom($nom) {
            $this->connexion();
            $nom = mysqli_real_escape_string($this->GetCnx(), $nom);
            $req = "select * from jeux where nom='".$nom."' LIMIT 1";
            $res = mysqli_query($this->GetCnx(),$req);
            $ligne = mysqli_fetch_assoc($res);
            if ($ligne) {
                $resultat = 1;
            } else {
                $resultat = null;
            }
            $this->deconnexion();
            return $resultat;
        }

        //Récupérer du nombre de jeux
        public function GetNbJeux() {
            $this->connexion();
            $req = "select COALESCE(MAX(id), 1) as cpt from jeux";
            $res = mysqli_query($this->GetCnx(),$req);
            $ligne = mysqli_fetch_assoc($res);
            if ($ligne) {
                $resultat = $ligne["cpt"];
            } else {
                $resultat = null;
            }
            $this->deconnexion();
            return $resultat;
        }

        public function AjouterJeu($nom, $acronyme, $petite_desc, $description, $image, $nbparticipants) {
            //On cherche à avoir le nouvel ID, avant le connexion pour ne pas le faire pendant un autre connexion.
            $nbJeux = $this->GetNbJeux()+1;
            $this->connexion();
            //mysqli_real_escape_string nécessaire pour éviter les erreurs si on rentre n'importe quoi.
            $nom = mysqli_real_escape_string($this->GetCnx(), $nom);
            $acronyme = mysqli_real_escape_string($this->GetCnx(), $acronyme);
            $petite_desc = mysqli_real_escape_string($this->GetCnx(), $petite_desc);
            $description = mysqli_real_escape_string($this->GetCnx(), $description);
            $image = mysqli_real_escape_string($this->GetCnx(), $image);
            $nbparticipants = mysqli_real_escape_string($this->GetCnx(), $nbparticipants);
            $jeu = new Jeux($nbJeux, $nom, $acronyme, $petite_desc, $description, $image, $nbparticipants);
            $req = "insert into jeux (nom, acronyme, petite_desc, description, image, nb_participants) values ('".$nom."','".$acronyme."','".$petite_desc."','".$description."','".$image."',".$nbparticipants.")";
            $ok = mysqli_query($this->GetCnx(), $req);
            if(!$ok) {
                $jeu = null;
            }
            $this -> deconnexion();
            //Si ça a fonctionné, on renvoi le jeu.
            return $jeu;
        }

        public function SupprimerJeu($id) {
            $game = $this->GetJeuById($id);
            $this->connexion();
            $id = mysqli_real_escape_string($this->GetCnx(), $id);
            if (!is_null($game)) {
                //On récupère l'image du jeu avant sa suppression, pour avoir son nom.
                $game = $game->GetImage();
            }
            $id = mysqli_real_escape_string($this->GetCnx(), $id);
            $req = "delete from jeux WHERE id=".$id;
            $jeu = $id;
            $ok = mysqli_query($this->GetCnx(), $req);
            if ($ok) {
                $jeu = null;
            }
            $this -> deconnexion();
            $this->alterAutoincrement("jeux");
            if (!is_null($game) && $ok) {
                //Sert à supprimer le fichier IMAGE du jeu !
                unlink("assets/img/games/".$game);
            }
            return $jeu;
        }

    }
