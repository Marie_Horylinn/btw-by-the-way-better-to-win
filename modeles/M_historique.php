<?php

    require_once "M_generique.php";
    require_once "metiers/Historique.php";

    class M_historique extends M_generique {

        public function GetClassement($idTournoi) {
            $resultat = array();
            $this->connexion();
            $idTournoi = mysqli_real_escape_string($this->GetCnx(), $idTournoi);
            $req = "SELECT DISTINCT id_equipe, place FROM historiquetournois where (id_tournois=".$idTournoi.") ORDER BY place asc";
            $res = mysqli_query($this->GetCnx(), $req);
            $ligne = mysqli_fetch_assoc($res);

            while ($ligne) {
                $groupe = new Historique($ligne["id_equipe"], 0, $idTournoi, $ligne["place"]);
                $resultat[] = $groupe;
                $ligne = mysqli_fetch_assoc($res);
            }
            $this->deconnexion();
            return $resultat;
        }

        public function GetHistoriqueListe() {
            $resultat = array();
            $this->connexion();
            $req = "select id_equipe, id_tournois, id_membre, place from historiquetournois";
            $res = mysqli_query($this->GetCnx(), $req);
            $ligne = mysqli_fetch_assoc($res);

            while ($ligne) {
                $groupe = new Historique($ligne["id_equipe"], $ligne["id_membre"], $ligne["id_tournois"], $ligne["place"]);
                $resultat[] = $groupe;
                $ligne = mysqli_fetch_assoc($res);
            }
            $this->deconnexion();
            return $resultat;
        }

        public function GetHistoriqueListeByEquipeID($IdEquipe) {
            $totalId = array();
            $resultat = array();
            $this->connexion();
            $IdEquipe = mysqli_real_escape_string($this->GetCnx(), $IdEquipe);
            $req = "select id_equipe, id_tournois, id_membre, place from historiquetournois where id_equipe=".$IdEquipe;
            $res = mysqli_query($this->GetCnx(), $req);
            $ligne = mysqli_fetch_assoc($res);

            while ($ligne) {
                if (!in_array($ligne["id_tournois"], $totalId)) {
                    $totalId[] = $ligne["id_tournois"];
                    $groupe = new Historique($ligne["id_equipe"], $ligne["id_membre"], $ligne["id_tournois"], $ligne["place"]);
                    $resultat[] = $groupe;
                }
                $ligne = mysqli_fetch_assoc($res);
            }
            $this->deconnexion();
            return $resultat;
        }

        public function AddHistorique($IdEquipe, $IdTournois, $IdMembre, $place) {
            $this->connexion();
            $IdEquipe = mysqli_real_escape_string($this->GetCnx(), $IdEquipe);
            $IdTournois = mysqli_real_escape_string($this->GetCnx(), $IdTournois);
            $IdMembre = mysqli_real_escape_string($this->GetCnx(), $IdMembre);
            $place = mysqli_real_escape_string($this->GetCnx(), $place);
            $participer = new Historique($IdEquipe, $IdMembre, $IdTournois, $place);
            $req = "insert into historiquetournois (id_equipe, id_tournois, id_membre, place) values (".$IdEquipe.","."$IdTournois".",".$IdMembre.",".$place.")";
            $ok = mysqli_query($this->GetCnx(), $req);
            if(!$ok) {
                $participer = null;
            }
            $this -> deconnexion();
            //Si ça a fonctionné, on renvoi l'historique.
            return $participer;
        }

        public function GetHistoriqueListeByMembreID($IdMembre) {
            $resultat = array();
            $this->connexion();
            $IdMembre = mysqli_real_escape_string($this->GetCnx(), $IdMembre);
            $req = "select id_equipe, id_tournois, id_membre, place from historiquetournois where id_membre=".$IdMembre;
            $res = mysqli_query($this->GetCnx(), $req);
            $ligne = mysqli_fetch_assoc($res);

            while ($ligne) {
                $groupe = new Historique($ligne["id_equipe"], $ligne["id_membre"], $ligne["id_tournois"], $ligne["place"]);
                $resultat[] = $groupe;
                $ligne = mysqli_fetch_assoc($res);
            }
            $this->deconnexion();
            return $resultat;
        }
    }
