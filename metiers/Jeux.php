<?php

class Jeux {
    private $jeu_id;
    private $jeu_nom;
    private $jeu_acronyme;
    private $jeu_petite_desc;
    private $jeu_desc;
    private $jeu_img;
    private $jeu_nbparticipantparequipe;

    public function __construct($id, $nom, $acronyme, $petite_desc, $desc, $img, $participantparequipe) {
        $this->jeu_id = $id;
        $this->jeu_nom = $nom;
        $this->jeu_acronyme = $acronyme;
        $this->jeu_petite_desc = $petite_desc;
        $this->jeu_desc = $desc;
        $this->jeu_img = $img;
        $this->jeu_nbparticipantparequipe = $participantparequipe;
    }

    public function GetId() {
        return $this->jeu_id;
    }

    public function GetNom() {
        return $this->jeu_nom;
    }

    public function GetAcronyme() {
        return $this->jeu_acronyme;
    }

    public function GetPetiteDesc() {
        return $this->jeu_petite_desc;
    }

    public function GetDesc() {
        return $this->jeu_desc;
    }

    public function GetImage() {
        return $this->jeu_img;
    }

    public function GetNbParticipantsParEquipe() {
        return $this->jeu_nbparticipantparequipe;
    }
}
