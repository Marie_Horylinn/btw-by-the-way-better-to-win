<?php

class Participer {
    private $participer_idEquipe;
    private $participer_idTournois;
    private $participer_idMembres;

    public function __construct($idEquipe, $idTournois, $idMembre) {
        $this->participer_idEquipe = $idEquipe;
        $this->participer_idTournois = $idTournois;
        $this->participer_idMembres = $idMembre;
    }

    public function GetIdEquipe() {
        return $this->participer_idEquipe;
    }

    public function GetIdTournois() {
        return $this->participer_idTournois;
    }

    public function GetIdMembre() {
        return $this->participer_idMembres;
    }
    
    public function __toString() {
        return $this->participer_idTournois;
    }
}
