<?php

class Membre {
    private $mem_id;
    private $mem_nom;
    private $mem_prenom;
    private $mem_mail;
    private $mem_tel;
    private $mem_pseudo;
    private $mem_mdp;
    private $mem_image;
    private $mem_idequipe;
    private $mem_idgroupe;

    public function __construct($id, $nom, $prenom, $mail, $tel, $pseudo, $mdp, $image, $idequipe, $idgroupe) {
        $this->mem_id = $id;
        $this->mem_nom = $nom;
        $this->mem_prenom = $prenom;
        $this->mem_mail = $mail;
        $this->mem_tel = $tel;
        $this->mem_pseudo = $pseudo;
        $this->mem_mdp = $mdp;
        $this->mem_image = $image;
        $this->mem_idequipe = $idequipe;
        $this->mem_idgroupe = $idgroupe;
    }

    public function GetId() {
        return $this->mem_id;
    }

    public function GetNom() {
        return $this->mem_nom;
    }

    public function GetPrenom() {
        return $this->mem_prenom;
    }

    public function GetMail() {
        return $this->mem_mail;
    }

    public function GetTel() {
        return $this->mem_tel;
    }

    public function GetPseudo() {
        return $this->mem_pseudo;
    }

    public function GetMdp() {
        return $this->mem_mdp;
    }

    public function GetImage() {
        return $this->mem_image;
    }

    public function GetIdEquipe() {
        return $this->mem_idequipe;
    }

    public function GetIdGroupe() {
        return $this->mem_idgroupe;
    }
}
