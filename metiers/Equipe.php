<?php

class Equipe {
    private $eqp_id;
    private $eqp_nom;
    private$eqp_description;

    public function __construct($id, $nom,$description) {
        $this->eqp_id = $id;
        $this->eqp_nom = $nom;
        $this->eqp_description = $description;
    }

    public function GetId() {
        return $this->eqp_id;
    }

    public function GetNom() {
        return $this->eqp_nom;
    }
    public function GetDescription() {
        return $this->eqp_description;
    }
}
