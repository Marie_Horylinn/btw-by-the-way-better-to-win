<?php

class Tournois {
    private $tournois_id;
    private $tournois_nom;
    private $tournois_dateHeure;
    private $tournois_nbEquipe;
    private $tournois_idJeu;

    public function __construct($id, $nom, $dateHeure, $nbEquipe, $idJeu) {
        $this->tournois_id = $id;
        $this->tournois_nom = $nom;
        $this->tournois_dateHeure = $dateHeure;
        $this->tournois_nbEquipe = $nbEquipe;
        $this->tournois_idJeu = $idJeu;
    }

    public function GetId() {
        return $this->tournois_id;
    }

    public function GetNom() {
        return $this->tournois_nom;
    }

    public function GetDateHeure() {
        return $this->tournois_dateHeure;
    }

    public function GetNbEquipe() {
        return $this->tournois_nbEquipe;
    }

    public function GetIdJeu() {
        return $this->tournois_idJeu;
    }
}
