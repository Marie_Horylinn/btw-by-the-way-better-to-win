<?php

class Groupe {
    private $grp_id;
    private $grp_libelle;

    //3 = Admin, 2 = Gérants, 1 = Joueur

    public function __construct($id, $libelle) {
        $this->grp_id = $id;
        $this->grp_libelle = $libelle;
    }

    public function GetId() {
        return $this->grp_id;
    }

    public function GetLibelle() {
        return $this->grp_libelle;
    }
}
