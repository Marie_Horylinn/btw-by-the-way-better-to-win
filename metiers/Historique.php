<?php

class Historique {
    private $historique_idEquipe;
    private $historique_idMembre;
    private $historique_idTournois;
    private $historique_placeTournois;

    public function __construct($idEquipe, $idMembre, $idTournois, $place) {
        $this->historique_idEquipe = $idEquipe;
        $this->historique_idMembre = $idMembre;
        $this->historique_idTournois = $idTournois;
        $this->historique_placeTournois = $place;
    }

    public function GetIdEquipe() {
        return $this->historique_idEquipe;
    }

    public function GetIdMembre() {
        return $this->historique_idMembre;
    }

    public function GetIdTournois() {
        return $this->historique_idTournois;
    }

    public function GetPlace() {
        return $this->historique_placeTournois;
    }
}
