<?php
session_start(); //On démarre la session ici, comme ça elle est déjà d'office sur toutes les pages !

require_once "controleurs/C_lemessage.php"; //Instanciation du conteneur de messages, pour l'avoir en bas à droite.
$controleurLeMessage = new C_lemessage();

//$_SESSION["isAdmin"] = true; //Définit si on est admin ou non, ça sera dans le Login ça après ! Il est là pour les test.
//unset($_SESSION["connecté"]); unset($_SESSION["membre"]); unset($_SESSION["idMembre"]);

include_once("vues/V_header.php"); //Balises html, meta...
include_once("vues/V_navbar.php"); //Navbar

if (!empty($_GET['page'])) {
	$page = $_GET['page'];
} else {
	$page = "accueil";
}
switch ($page) {

	/*Accueil*/
	case "accueil":
		require_once "controleurs/C_accueil.php";
		$controleur = new C_accueil();
		$controleur->action_afficherAccueil();
		break;

	/*Afficher Inscription*/
	case "inscription":
		include_once("controleurs/C_inscription.php");
		$controleur = new C_inscription();
		$controleur->action_afficher();
		break;

	/*Afficher Connexion*/
	case "connexion":
		include_once("controleurs/C_connexion.php");
		$controleur = new C_connexion();
		$controleur->action_saisie();
		break;

	/*Procédure Inscription Membre*/
	case "ajoutmembre":
		require_once("controleurs/C_inscription.php");
		$controleur = new C_inscription();
		if (
			isset($_POST["nom"]) && isset($_POST["prenom"]) && isset($_POST["mail"]) && isset($_POST["tel"]) &&
			isset($_POST["pseudo"]) && isset($_POST["mdp"]) && isset($_POST["choix_equipe"]) && isset($_POST["choix_categ"])
		)
			$controleur->action_ajout($_POST["nom"], $_POST["prenom"], $_POST["mail"], $_POST["tel"], $_POST["pseudo"], $_POST["mdp"], "profilbase.png", $_POST["choix_equipe"], $_POST["choix_categ"]);
		else
			include_once("vues/V_error404.php");
		break;

	/*Procédure Inscription Gérant*/
	case "ajoutgerant":
		require_once("controleurs/C_inscription.php");
		$controleur = new C_inscription();
		if (
			isset($_POST["nom"]) && isset($_POST["prenom"]) && isset($_POST["mail"]) && isset($_POST["tel"]) &&
			isset($_POST["pseudo"]) && isset($_POST["mdp"]) && isset($_POST["nom_equipe"]) && isset($_POST["choix_categ"]) &&
			isset($_POST["description_equipe"]) &&  isset($_FILES["profileImageJeu"]['tmp_name'])
		)
			$controleur->action_ajoutGerant($_POST["nom"], $_POST["prenom"], $_POST["mail"], $_POST["tel"], $_POST["pseudo"], $_POST["mdp"], "profilbase.png", "0", $_POST["choix_categ"], $_POST["nom_equipe"], $_POST["description_equipe"], $_FILES["profileImageJeu"]);
		else
			include_once("vues/V_error404.php");
		break;

	/*Procédure Connexion*/
	case "connexionmembre":
		require_once("controleurs/C_connexion.php");
		$controleur = new C_connexion();
		if (isset($_POST["pseudo"]) && isset($_POST["mdp"]))
			$controleur->action_connexion($_POST["pseudo"], $_POST["mdp"]);
		else
			include_once("vues/V_error404.php");
		break;

	/*Afficher Jeux*/
	case "jeux":
		require_once "controleurs/C_consulterJeux.php";
		$controleur = new C_consulterJeux();
		/*
			* On vérifie si on a bien obtenu en paramètre les ID ou Acronyme
			* Dans le cas contraire on peut les mettre Null, car ils peuvent être null dans action_afficherJeu
			* Si les deux sont nuls, AfficherJeu affichera Erreur 404 !
			*/
		if (isset($_GET["id"]))
			$id = $_GET["id"];
		else
			$id = null;
		if (isset($_GET["acronym"]))
			$acronym = $_GET["acronym"];
		else
			$acronym = null;
		$controleur->action_afficherJeu($id, $acronym);
		break;

	/*Procédure Ajouter un Jeu*/
	case "ajoutjeu":
		require_once "controleurs/C_ajouterJeux.php";
		$controleur = new C_ajouterJeux();
		/*
			* On vérifie si on a bien obtenu d'un formulaire post toutes nos donénes
			* Si ce n'est pas le cas, Erreur 404.
			*/
		if (
			isset($_POST["titreJeu"]) && isset($_POST["acronymeJeu"]) && isset($_POST["petiteDescJeu"]) && isset($_POST["DescJeu"])
			&& isset($_POST["nbParticipantsJeu"]) && isset($_FILES["profileImageJeu"]['tmp_name'])
		) {
			$titre = $_POST["titreJeu"];
			$acro = $_POST["acronymeJeu"];
			$pttdesc = $_POST["petiteDescJeu"];
			$desc = $_POST["DescJeu"];
			$nbparts = $_POST["nbParticipantsJeu"];
			$imgName = $_FILES["profileImageJeu"];
			$controleur->action_ajout($titre, $acro, $pttdesc, $desc, $imgName, $nbparts);
		} else {
			include_once("vues/V_error404.php");
		}
		break;

	/*Procédure supprimer un membre*/
	case "supprimer":
		require_once "controleurs/C_supprimerMembre.php";
		if (isset($_SESSION["isAdmin"]) && $_SESSION["isAdmin"] == true) {
			if (isset($_GET["idMembre"])) {
				$controleur = new C_supprimerMembre;
				$controleur->action_supprimer($_GET["idMembre"]);
			}
		}
		include_once("vues/V_error404.php");
		break;

	/*Procédure Supprimer un jeu*/
	case "supprimerjeu":
		require_once "controleurs/C_supprimerJeux.php";
		$controleur = new C_supprimerJeux();
		$idJeu = null;
		/*
			* On vérifie si on a obtenu listeJeux en paramètre de formulaire
			* Et on récupère le String qui est avant le premier Espace. (Explode = Split en C#)
			* Donc $separateur[0] récupère le 1er argument de Split, pour le mettre dans Action_suppimer
			* S'il n'est pas bon, action_supprimer mettra l'erreur 404.
			*/
		if (isset($_POST["listeJeux"])) {
			$separateur = explode(" ", $_POST["listeJeux"]);
			$idJeu = $separateur[0];
		}
		$controleur->action_supprimer($idJeu);
		break;

		break;

	
	/*Procédure afficher liste tournois*/
	case "listetournois":
		require_once("controleurs/C_consulterTournois.php");
		$controleur = new C_consulterTournois();
		$controleur->action_listeTournois();
		break;

	/*Procédure ajouter tournois*/
	case "ajouttournois":
		require_once("controleurs/C_gestionTournois.php");
		$controleur = new C_gestionTournois();
		if (
			isset($_POST["jeuTournoi"]) && isset($_POST["nbParticipantsTournoi"]) &&
			isset($_POST["dateHeureTournoi"]) && isset($_POST["nomTournoi"])
		) {
			$controleur->action_creerTournois($_POST["nomTournoi"], $_POST["jeuTournoi"], $_POST["nbParticipantsTournoi"], $_POST["dateHeureTournoi"]);
		} else {
			include_once("vues/V_error404.php");
		}
		break;

	/*Procédure supprimer tournois*/
	case "supprimertournois":
		require_once("controleurs/C_gestionTournois.php");
		$controleur = new C_gestionTournois();
		if (isset($_POST["listeTournois"])) {
			$separateur = explode(" ", $_POST["listeTournois"]);
			$id = $separateur[0];
			$controleur->action_supprimerTournois($id);
		} else {
			include_once("vues/V_error404.php");
		}
		break;

	/*Afficher les résulats du tournoi*/
	case "resultattournois":
		require_once("controleurs/C_resultatTournois.php");
		$controleur = new C_resultatTournois;
		if (isset($_GET["idTournois"]))
			$controleur->action_afficherResultatTournois($_GET["idTournois"]);
		else
			include_once("vues/V_error404.php");
		break;

	/*Afficher les tournois me concernant et historique*/
	case "mestournois":
		require_once("controleurs/C_mesTournois.php");
		$controleur = new C_mesTournois();
		$controleur->action_afficher();
		break;

	/*Afficher règlement*/
	case "reglement":
		require_once("vues/V_reglement.php");
		break;

	/*Afficher profil*/
	case "profil":
		require_once("controleurs/C_profil.php");
		$controleur = new C_profil();
		$controleur->action_recup();
		break;

	/*Procédure déconnexion*/
	case "deconnexion":
		require_once("controleurs/C_connexion.php");
		$controleur = new C_connexion();
		$controleur->action_deconnexion();
		break;

	/*Procédure modifier profil*/
	case "modifierProfil":
		require_once("controleurs/C_profil.php");
		$controleur = new C_profil();
		if (
			isset($_POST["modif_id"]) && isset($_POST["modif_nom"]) && isset($_POST["modif_prenom"]) && isset($_POST["modif_mail"]) &&
			isset($_POST["modif_tel"]) && isset($_POST["modif_pseudo"]) && isset($_POST["modif_mdp"]) && isset($_FILES["modif_image"]['tmp_name']))
		 {
			if (isset($_POST["modif_equipe"])) {
				//Si on est un membre, alors on actione ce mécanisme
				$controleur->action_modif($_POST['modif_id'], $_POST["modif_nom"], $_POST["modif_prenom"], $_POST["modif_mail"], $_POST["modif_tel"], $_POST["modif_pseudo"], $_POST["modif_mdp"], $_FILES["modif_image"], $_POST['modif_equipe']);
			} else {
				//Si on est un gérant, c'est ici.
				$controleur->action_modif($_POST['modif_id'], $_POST["modif_nom"], $_POST["modif_prenom"], $_POST["modif_mail"], $_POST["modif_tel"], $_POST["modif_pseudo"], $_POST["modif_mdp"], $_FILES["modif_image"], null);
			}
			
		}
		else
			include_once("vues/V_error404.php");
		break;

	/*Procédure inscription tournoi*/
	case "inscriptiontournoi":
		if (isset($_SESSION["groupe"]) && $_SESSION["groupe"] != 3 && isset($_GET["idTournois"])) {
			require_once("controleurs/C_inscriptionTournois.php");
			$controleur = new C_inscriptionTournois;
			$controleur->action_inscriptionTournois($_GET["idTournois"]);
		} else {
			include_once("vues/V_error404.php");
		}
		break;

	/*Afficher page admin*/
	case "admin":
		if (isset($_SESSION["isAdmin"]) && $_SESSION["isAdmin"] == true) {
			require_once("controleurs/C_admin.php");
			$controleur = new C_admin;
			$controleur->action_afficher();
		} else {
			include_once("vues/V_error404.php");
		}
		break;
	
	default:
		//La page par défaut est Erreur 404.
		include_once("vues/V_error404.php");
		break;
}

/*
	* ça c'est pour fonctionner avec les leMessage qui s'affichent en bas à droite.
	* En gros vu qu'on doit passer par 3 fichiers, et qu'on utilise pas Header, j'ai du cafouiller
	* avec du Javascript et le $_SESSION pour partager les infos.
	*
	* Donc là il va vérifier si la valeur "lemessagelife" est remplie, si c'est le cas et qu'elle est
	* et que les calculs d'en dessous sont bons, alors elle va supprimer toutes les autres valeurs du leMessage
	* comme la couleur, le lien de renvoi, le titre, et le message en lui même.
	*/
if (isset($_SESSION["lemessagelife"])) {
	$autokill = 10; //Temps en seconde avant que les valeurs se suppriment (Il fallait minimum 5 secondes le temps de changement de page)
	$session_lemassage_life = time() - $_SESSION["lemessagelife"];
	if ($session_lemassage_life > $autokill) {
		unset($_SESSION["colorleMessage"]);
		unset($_SESSION["lienleMessage"]);
		unset($_SESSION["titreleMessage"]);
		unset($_SESSION["leMessage"]);
		unset($_SESSION["lemessagelife"]);
	}
}
//On va dans C_lemessage.php pour la fonction.
$controleurLeMessage->action_afficherLeMessage();

include_once("vues/V_footerPage.php");
include_once("vues/V_footer.php");
