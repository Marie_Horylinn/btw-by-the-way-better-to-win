      <!-- Modal -->
      <?php
      /*
        * On utilise les Modal de bootstrap pour faire notre page.
        */
      ?>
      <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="staticBackdropLabel">Ajouter un jeu</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="index.php?page=ajoutjeu" method="POST" enctype="multipart/form-data">
              <div class="modal-body">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
                <div class="container-fluid">
                  <div class="mb-3">
                    <label for="titreJeu" class="form-label">Titre:</label>
                    <input type="text" name="titreJeu" class="form-control" id="inputtitreJeu" aria-describedby="titreHelp" minlength="4" maxlength="250" required>
                    <div class="form-text" id="titreHelp"></div>
                  </div>
                  <div class="mb-3">
                    <label for="acronymeJeu" class="form-label">Acronyme:</label>
                    <input type="text" name="acronymeJeu" class="form-control" id="inputacronymeJeu" aria-describedby="acronymeHelp" minlength=2 maxlength="4" required>
                    <div class="form-text" id="acronymeHelp"></div>
                  </div>
                  <div class="mb-3">
                    <label for="petiteDescJeu" class="form-label">Description rapide:</label>
                    <input type="text" name="petiteDescJeu" class="form-control" id="inputpetiteDescJeu" aria-describedby="petiteDescHelp" required>
                    <div class="form-text" id="petiteDescHelp"></div>
                  </div>
                  <div class="mb-3">
                    <label for="DescJeu" class="form-label">Description complète:</label>
                    <input type="text" name="DescJeu" class="form-control" id="inputDescJeu" aria-describedby="DescHelp" required>
                    <div class="form-text" id="DescHelp"></div>
                  </div>
                  <div class="mb-3">
                    <label for="nbParticipantsJeu" class="form-label">Nombre de participants:</label>
                    <input type="number" name="nbParticipantsJeu" class="form-control" id="inputnbParticipantsJeu" aria-describedby="nbParticipantsHelp" min="3" required>
                    <div class="form-text" id="nbParticipantsHelp"></div>
                  </div>
                  <div class="mb-3">
                    <label for="profileImageJeu" class="form-label">Image:</label>
                    <input id="imageUpload" name="profileImageJeu" class="form-control" type="file" accept="image/jpeg" placeholder="Photo" required="" capture required>
                    <image id="profileImage" src="" class="my-3 img-thumbnail profileImg" />
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Ajouter le jeu</button>
                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Annuler</button>
              </div>
            </form>
          </div>
        </div>
        <script>
          /*
           * On utilise Ajax pour afficher un aperçu de notre image séléctionnée lors de la création
           * Ajax est un framework javascript .w.
           */
          $("#profileImage").click(function(e) {
            $("#imageUpload").click();
          });

          function fasterPreview(uploader) {
            if (uploader.files && uploader.files[0]) {
              $('#profileImage').attr('src', window.URL.createObjectURL(uploader.files[0]));
            }
          }

          $('#imageUpload').change(function() {
            fasterPreview(this);
          })
        </script>
      </div>