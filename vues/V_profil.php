<?php require_once("modeles/M_groupe.php");
$controleur = new c_profil();
?>
<div class="container-fluid bg-dark bg-gradient text-center p-5">
  <div class="bg-dark" style="border:2px solid white;">
    <div class="display-5 fw-bold text-center text-white p-3 m-3" style="border-bottom:2px solid white;"> Mon profil</div>
    <div class="row">
      <div class="col-6 py-5">
        <h2 class="display-5 fw-bold text-center text-white mb-0 pb-5"><?php echo $this->data['leMembre']->GetPseudo(); ?> </h2>
        <img src="assets/img/profil/<?php echo $this->data['leMembre']->GetImage(); ?>" class="img-thumbnail" style="border-radius:120px;" />
      </div>


      <div class="col-6 py-5" style="border-left:2px solid white;">
        <h2 class="display-5 fw-bold text-center text-white mb-0 pb-5">Mes informations</h2>
        <div class="row text-left">
          <div class="col-6">
            <p class="text-white text-start ps-5">
              <?php echo "<strong>Nom :</strong> ", $this->data['leMembre']->GetPrenom(), " ", $this->data['leMembre']->GetNom(); ?>
            </p>
            <p class=" text-white text-start ps-5">
              <?php echo "<strong>Mail : </strong>", $this->data['leMembre']->GetMail(); ?>
            </p>
            <p class="text-white text-start ps-5">
              <?php echo "<strong>Tél : </strong>", $this->data['leMembre']->GetTel(); ?>
            </p>
          </div>
          <div class="col-6">
            <p class="text-end text-white pe-5">
              <?php echo "Equipe : ", $this->data['lequipe']->GetNom(); ?>
            </p>
            <p class="text-end text-white pe-5">
              <?php echo "Rôle : ", $this->data['leGroupe']->GetLibelle(); ?>
            </p>
          </div>
        </div>
        <a class="btn btn-warning" href="#modifierProfil" data-bs-toggle="modal">Modifier mes informations</a>
      </div>
    </div>
  </div>
</div>


<!-- MODAL MOFIFICATION PROFIL -->
<div class="modal" tabindex="-1" id="modifierProfil">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modifier mes informations</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form action="index.php?page=modifierProfil" method="POST" enctype="multipart/form-data">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <div class="modal-body">
          <input type="hidden" class="form-control" name="modif_id" value="<?php echo $this->data['leMembre']->GetId(); ?>">
          <input type="hidden" class="form-control" name="modif_equipeid" value="<?php echo $this->data['lequipe']->GetId(); ?>">
          <input type="hidden" class="form-control" name="modif_gpid" value="<?php echo $this->data['leGroupe']->GetId(); ?>">
          <div class="mb-3">
            <label for="exampleFormControlInput1" class="form-label">Pseudo</label>
            <input type="text" class="form-control" name="modif_pseudo" value="<?php echo $this->data['leMembre']->GetPseudo(); ?>" required>
          </div>
          <div class="mb-3">
            <label for="exampleFormControlInput1" class="form-label">Nom</label>
            <input type="text" class="form-control" name="modif_nom" value="<?php echo $this->data['leMembre']->GetNom(); ?>" required>
          </div>
          <div class="mb-3">
            <label for="exampleFormControlInput1" class="form-label">Prénom</label>
            <input type="text" class="form-control" name="modif_prenom" value="<?php echo $this->data['leMembre']->GetPrenom(); ?>" required>
          </div>
          <div class="mb-3">
            <label for="exampleFormControlInput1" class="form-label">Mail</label>
            <input type="email" class="form-control" name="modif_mail" value="<?php echo $this->data['leMembre']->GetMail(); ?>" required>
          </div>
          <div class="mb-3">
            <label for="exampleFormControlInput1" class="form-label">Tél</label>
            <input type="text" class="form-control" name="modif_tel" value="<?php echo $this->data['leMembre']->GetTel(); ?>" required>
          </div>
          <div class="mb-3">
            <label for="exampleFormControlInput1" class="form-label">Photo de profil</label>
            <input id="imageUpload" type="file" class="form-control" accept="image/jpeg" name="modif_image" required>
            <image id="profileImage" src="" class="my-3 img-thumbnail profileImg" />
          </div>
          <div class="mb-3">
            <label for="exampleFormControlInput1" class="form-label">Mot de passe</label>
            <input type="password" class="form-control" name="modif_mdp" required>
          </div>
          <?php if ($this->data["leMembre"]->GetIdGroupe() == 3) { ?>
          <div class="mb-3">
            <label for="lesEquipes" class="form-label">Equipe</label>
            <select class="form-select" name="modif_equipe">
                <?php 
                  
                  foreach ($this->data['lesEquipes'] as $uneEquipe) {
                ?>
                <option value="<?php echo $uneEquipe->GetId(); ?>" <?php if ($this->data["leMembre"]->GetIdEquipe() == $uneEquipe->GetId()) echo "selected" ?>><?php echo $uneEquipe->GetNom(); ?></option><?php
                                                                                                        }
                                                                                                          ?>
            </select>
          </div>
          <?php } ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Modifier</button>
        </div>
      </form>
    </div>
  </div>
  <script>
    /*
     * On utilise Ajax pour afficher un aperçu de notre image séléctionnée lors de la création
     * Ajax est un framework javascript .w.
     */
    $("#profileImage").click(function(e) {
      $("#imageUpload").click();
    });

    function fasterPreview(uploader) {
      if (uploader.files && uploader.files[0]) {
        $('#profileImage').attr('src', window.URL.createObjectURL(uploader.files[0]));
      }
    }

    $('#imageUpload').change(function() {
      fasterPreview(this);
    })
  </script>
</div>
<!-- FIN MODAL -->