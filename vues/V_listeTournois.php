<?php
//Si on a aucun jeu en paramètre de data (si on ouvre la page sans avoir d'id, ou un mauvais), alors erreur 404 !
if (is_null($this->data['lesTournois']) || is_null($this->data['lesJeux'])) {
    require_once("vues/V_error404.php");
} else {
?>
    <!--Bannière début-->
    <div class="bg-principal">
        <?php
        echo "<section class='py-5 text-center container bg-image-game' style='background: linear-gradient(to bottom, rgba(255,255,0,0.5), rgba(0,0,255,0.5)), url(\"assets/img/FooterImg.jpg\")';>";
        ?>
        <div class="row py-lg-5">
            <div class="col-lg-6 col-md-8 mx-auto">
                <h1 class="fw-light text-white mt-5 pt-5 fw-bold">Nos Tournois</h1>
                <p>
                    <?php
                    foreach ($this->data['lesJeuxEnregister'] as $jeux) {
                    ?>
                        <a href="#nostournois<?= $jeux->GetNom(); ?>" class="btn btn-primary mt-2"><?= $jeux->GetNom(); ?> »</a>
                    <?php
                    }
                    ?>
                </p>
                <p>
                    <!-- Button trigger modal -->
                    <?php
                    /*
                        * On utilise les Session pour savoir si on est admin ou non, pour savoir si on doit afficher les boutons Ajouter & Supprimer jeu !
                        */
                    if (isset($_SESSION['isAdmin']) && $_SESSION["isAdmin"] == true) {
                    ?>
                        <a class="btn btn-success mt-2" data-bs-toggle="modal" href="#staticBackdropCreeTournoi">
                            Créer un tournoi
                        </a>
                        <a class="btn btn-danger mt-2" data-bs-toggle="modal" href="#staticBackdropSupprimerTournoi">
                            Supprimer un tournoi
                        </a>
                        <?php include_once "vues/V_saisieTournois.php"; ?>
                        <?php include_once "vues/V_supprimerTournois.php"; ?>
                    <?php
                    }
                    ?>
                </p>
            </div>
        </div>
    </div>

    <?php
    $i = 0;
    foreach ($this->data['lesJeuxEnregister'] as $jeux) {
        if ($i == 0) {
            $i = 1;
            echo '<div class="bg-principal">';
        } else {
            $i = 0;
            echo '<div class="bg-secondaire">';
        }
    ?>
        <div class="container py-5" id="nostournois<?= $jeux->GetNom(); ?>">
            <h1 class="display-5 fw-bold text-white text-center mb-5">Tournois - <?= $jeux->GetNom(); ?></h1>
            <div class="row row-cols-1 row-cols-md-3 g-4 pt-5">
                <?php
                foreach ($this->data['lesTournois'] as $tournois) {
                    if ($tournois->GetIdJeu() == $jeux->GetId()) {
                ?>

                        <div class="col">
                            <div class="card h-100 text-center">
                                <h5 class="card-header">#<?= $tournois->GetId(); ?></h5>
                                <div class="card-body">
                                    <h5 class="card-title">Nom: <?= $tournois->GetNom(); ?></h5>
                                    <p class="card-text">Jeu: <?= $jeux->GetNom(); ?></p>
                                </div>
                                <div class="card-body">
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">Date de début: <?= $tournois->GetDateHeure(); ?></li>
                                        <li class="list-group-item">Nombre d'équipe: <?= $tournois->GetNbEquipe(); ?></li>
                                        <li class="list-group-item">Participants par équipes: <?= $jeux->GetNbParticipantsParEquipe(); ?></li>
                                    </ul>
                                    <?php
                                    if (strtotime($tournois->GetDateHeure()) < time()) {
                                        echo '<a href="index.php?page=resultattournois&idTournois=' . $tournois->GetId() . '" class="btn btn-warning">Voir les résultats</a>';
                                    } else {
                                        if (isset($_SESSION["groupe"]) && $_SESSION["groupe"] != 3) {
                                            if (in_array($tournois->GetId(), $this->data["lesParticipations"])) {
                                                echo '<a href="#" class="btn btn-success">Vous êtes inscrit</a>';
                                            } else {
                                                echo '<a href="index.php?page=inscriptiontournoi&idTournois=' . $tournois->GetId() . '" class="btn btn-info">Inscription</a>';
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>

                <?php
                    }
                }
                ?>
            </div>
        </div>
        </div>

    <?php
    }

    ?>
<?php
}
?>