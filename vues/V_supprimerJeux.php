      <!-- Modal -->
      <div class="modal fade" id="deleteGameBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="deleteGameBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="deleteGameBackdropLabel">Supprimer un jeu</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="index.php?page=supprimerjeu" method="POST" enctype="multipart/form-data">
              <div class="modal-body">
                <div class="container-fluid">
                  <div class="mb-3">
                    <label for="listeJeux" class="form-label">Jeux disponibles:</label>
                    <input class="form-control" name="listeJeux" list="listeJeuxOptions" id="listeJeuxList" placeholder="Chercher un jeu..." required>
                    <datalist id="listeJeuxOptions">
                      <?php
                      foreach ($this->data['lesJeux'] as $unJeu) {
                        /*
                              * Ici on écho l'ID du jeu, Espace,Son Nom. Pour pouvoir avoir son ID dans notre Index.php, case SupprimerJeu
                              */
                        echo "<option value='" . $unJeu->GetId() . " - " . $unJeu->GetNom() . "'>";
                      }
                      ?>
                    </datalist>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Supprimer le jeu</button>
                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Annuler</button>
              </div>
            </form>
          </div>
        </div>
      </div>