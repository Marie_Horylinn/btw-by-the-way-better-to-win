<?php
//Si on a aucun jeu en paramètre de data (si on ouvre la page sans avoir d'id, ou un mauvais), alors erreur 404 !
if (is_null($this->data['lesTournois']) || is_null($this->data['lesJeux'])) {
    require_once("vues/V_error404.php");
} else {
?>
    <!--Bannière début-->
    <div class="bg-principal">
        <?php
        echo "<section class='py-5 text-center container bg-image-game' style='background: linear-gradient(to bottom, rgba(255,255,0,0.5), rgba(0,0,255,0.5)), url(\"assets/img/FooterImg.jpg\")';>";
        ?>
        <div class="row py-lg-5">
            <div class="col-lg-6 col-md-8 mx-auto">
                <h1 class="fw-light text-white mt-5 pt-5 fw-bold">Mes Tournois</h1>
                <p>
                    <a href="#actuels" class="btn btn-success mt-2">En cours »</a>
                    <a href="#historiqueequipe" class="btn btn-primary mt-2">Historique de mon équipe »</a>
                    <a href="#historiquepersonnel" class="btn btn-info mt-2">Mon historique »</a>
                </p>
            </div>
        </div>
    </div>

    <div class="bg-secondaire" id="actuels">
        <div class="container py-5">
            <h1 class="display-5 fw-bold text-white text-center mb-5">Tournois inscrits par votre équipe</h1>
            <div class="row row-cols-1 row-cols-md-3 g-4 pt-5">
                <?php
                if (!empty($this->data["TournoisEquipe"])) {
                    foreach ($this->data["TournoisEquipe"] as $participer) {
                        $tournoi = $this->data["lesTournois"][$participer->GetIdTournois() - 1];
                        $jeu = $this->data["lesJeux"][$tournoi->GetIdJeu() - 1];
                ?>
                        <div class="col">
                            <div class="card h-100 text-center">
                                <?php echo '<img src="assets/img/games/' . $jeu->GetImage() . '" class="card-img-top" alt="...">'; ?>
                                <h5 class="card-header">#<?= $tournoi->GetId(); ?></h5>
                                <div class="card-body">
                                    <h5 class="card-title">Nom: <?= $tournoi->GetNom(); ?></h5>
                                    <p class="card-text">Jeu: <?= $jeu->GetNom(); ?></p>
                                </div>
                                <div class="card-body">
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">Date de début: <?= $tournoi->GetDateHeure(); ?></li>
                                        <li class="list-group-item">Nombre d'équipe: <?= $tournoi->GetNbEquipe(); ?></li>
                                        <li class="list-group-item">Participants par équipes: <?= $jeu->GetNbParticipantsParEquipe(); ?></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                } else {
                    ?>
                    <p class="text-white">Votre équipe n'a actuellement pas de tournois en attente.</p>
                <?php
                }
                ?>
            </div>
        </div>
    </div>

    <div class="bg-principal" id="historiqueequipe">
        <div class="container py-5">
            <h1 class="display-5 fw-bold text-white text-center mb-5">Historique de tournoi de votre équipe</h1>
            <div class="row row-cols-1 row-cols-md-3 g-4 pt-5">
                <?php
                if (!empty($this->data["HistoriqueEquipe"])) {
                    foreach ($this->data["HistoriqueEquipe"] as $participer) {
                        $tournoi = $this->data["lesTournois"][$participer->GetIdTournois() - 1];
                        $jeu = $this->data["lesJeux"][$tournoi->GetIdJeu() - 1];
                ?>
                        <div class="col">
                            <div class="card h-100 text-center">
                                <?php echo '<img src="assets/img/games/' . $jeu->GetImage() . '" class="card-img-top" alt="...">'; ?>
                                <h5 class="card-header">#<?= $tournoi->GetId(); ?></h5>
                                <div class="card-body">
                                    <h5 class="card-title">Nom: <?= $tournoi->GetNom(); ?></h5>
                                    <p class="card-text">Jeu: <?= $jeu->GetNom(); ?></p>
                                </div>
                                <div class="card-body">
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">Date de début: <?= $tournoi->GetDateHeure(); ?></li>
                                        <li class="list-group-item">Nombre d'équipe: <?= $tournoi->GetNbEquipe(); ?></li>
                                        <li class="list-group-item">Participants par équipes: <?= $jeu->GetNbParticipantsParEquipe(); ?></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                } else {
                    ?>
                    <p class="text-white">Votre équipe n'a pas de tournois en historique.</p>
                <?php
                }
                ?>
            </div>
        </div>
    </div>

    <div class="bg-secondaire" id="historiquepersonnel">
        <div class="container py-5">
            <h1 class="display-5 fw-bold text-white text-center mb-5">Votre Historique de tournois</h1>
            <div class="row row-cols-1 row-cols-md-3 g-4 pt-5">
                <?php
                if (!empty($this->data["HistoriqueMembre"])) {
                    foreach ($this->data["HistoriqueMembre"] as $participer) {
                        $tournoi = $this->data["lesTournois"][$participer->GetIdTournois() - 1];
                        $jeu = $this->data["lesJeux"][$tournoi->GetIdJeu() - 1];
                ?>
                        <div class="col">
                            <div class="card h-100 text-center">
                                <?php echo '<img src="assets/img/games/' . $jeu->GetImage() . '" class="card-img-top" alt="...">'; ?>
                                <h5 class="card-header">#<?= $tournoi->GetId(); ?></h5>
                                <div class="card-body">
                                    <h5 class="card-title">Nom: <?= $tournoi->GetNom(); ?></h5>
                                    <p class="card-text">Jeu: <?= $jeu->GetNom(); ?></p>
                                </div>
                                <div class="card-body">
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">Date de début: <?= $tournoi->GetDateHeure(); ?></li>
                                        <li class="list-group-item">Nombre d'équipe: <?= $tournoi->GetNbEquipe(); ?></li>
                                        <li class="list-group-item">Participants par équipes: <?= $jeu->GetNbParticipantsParEquipe(); ?></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                } else {
                    ?>
                    <p class="text-white">Vous n'avez pas de tournois dans votre historique.</p>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
<?php
}
?>