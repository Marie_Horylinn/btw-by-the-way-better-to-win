<div class="bg-principal">
    <div class="container py-5">
        <p class="fw-bold text-white text-center mb-5">
            Bienvene sur votre Panel, vous pouvez voir les informations des membres, et les supprimer.
        </p>
        <?php if (!empty($this->data['lesMembres'])) { ?>

            <table class="table text-white">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nom</th>
                        <th scope="col">Prénom</th>
                        <th scope="col">Mail</th>
                        <th scope="col">Tel</th>
                        <th scope="col">Pseudo</th>
                        <th scope="col">Photo</th>
                        <th scope="col">Groupe</th>
                        <th scope="col">Equipe</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 0;
                    foreach ($this->data["lesMembres"] as $membres) { ?>
                        <tr class="text-white">
                            <th scope="row"><?= $membres->GetId(); ?></th>
                            <td><?= $membres->GetNom(); ?></td>
                            <td><?= $membres->GetPrenom(); ?></td>
                            <td><?= $membres->GetMail(); ?></td>
                            <td><?= $membres->GetTel(); ?></td>
                            <td><?= $membres->GetPseudo(); ?></td>
                            <td><?php echo '<img src="assets/img/profil/' . $membres->GetImage() . '" class="img-fluid img-thumbnail" style="height:50px;" alt="...">' ?></td>
                            <td><?= $this->modeleEquipe->GetEquipeById($membres->GetIdEquipe())->GetNom(); ?></td>
                            <td><?= $this->modeleGroupe->GetGroupeById($membres->GetIdGroupe())->GetLibelle(); ?></td>
                            <?php
                            if ($membres->GetIdGroupe() != 1) { ?>
                                <td><a href="index.php?page=supprimer&idMembre=<?= $membres->GetId(); ?>" class="btn btn-danger mt-2">Supprimer</a>
                            <?php } else {
                                echo "<td>Compte admin</td>";
                            }
                                ?>

                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        <?php } else {
            echo "<br/><p class='text-white text-center'>Aucun membre n'a été enregistré, même pas vous, c'est bizarre..</p>";
        } ?>
    </div>
</div>