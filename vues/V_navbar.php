<div class="position-sticky bg-principal" id="top">

  <!--Navbar 1-->
  <nav class="navbar navbar-expand-lg navbar-dark">
    <div class="container-fluid nav ">
      <a class="navbar-brand txtlogo text-light" href="index.php"><img src="assets/img/BTW_Esport.png" height="90px" width="90px" />BTW²</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse " id="navbarSupportedContent">
        <ul class="nav navbar-nav ms-auto itemnavbar">
          <li class="nav-item ">
            <a class="nav-link active text-light" aria-current="page" href="index.php">Accueil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-light" href="index.php#actus">Actualités</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-light" href="index.php#nosjeux">Nos jeux</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-light" href="index.php?page=listetournois">Tournois</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-light" href="index.php?page=reglement">Règles</a>
          </li>
          <li>
            <hr class="dropdown-divider text-light">
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!--Navbar 2-->
  <div class="bg-secondaire">
    <nav class="navbar navbar-expand-lg navbar-light">
      <div class="collapse navbar-collapse container-fluid nav ">
        <div class="collapse navbar-collapse" id="navbarNavDarkDropdown">
          <ul class="navbar-nav text">
            <?php
            if (isset($_SESSION['connecté']) && $_SESSION['connecté'] == true) {
            ?>
              <li class="nav-item dropdown navbar-right">
                <a class="nav-link dropdown-toggle text-white navbar-right" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Profil
                </a>
                <ul class="dropdown-menu dropdown-menu-dark navbar-right" aria-labelledby="navbarDarkDropdownMenuLink">
                  <li><a class="dropdown-item text-white navbar-right" href="index.php?page=profil">Voir mon profil</a></li>
                  <li><a class="dropdown-item text-white navbar-right" href="index.php?page=mestournois">Mes tournois</a></li>
                  <?php
                  if (isset($_SESSION["isAdmin"]) && $_SESSION["isAdmin"] == true) {
                  ?>
                    <li><a class="dropdown-item text-white navbar-right" href="index.php?page=admin">Panel Admin</a></li>
                  <?php
                  }
                  ?>
                  <li><a class="dropdown-item text-white navbar-right" href="index.php?page=deconnexion">Me déconnecter</a></li>
                </ul>
              </li>
          </ul>
        </div>


      <?php
            } else {

      ?>
        <li class="nav-item">
          <a class="nav-link navbar-right text-light" href="index.php?page=connexion">Connexion</a>
        </li>
        <li class="nav-item">
          <a class="nav-link navbar-right text-light" href="index.php?page=inscription">Inscription</a>
        </li>
      <?php
            } ?>
      </div>
  </div>
  </nav>
</div>