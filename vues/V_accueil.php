<!--Bannière début-->
<section class="py-5 text-center container bg-image">
  <div class="row py-lg-5">
    <div class="col-lg-6 col-md-8 mx-auto">
      <h1 class="fw-light text-white mt-5 pt-5">BTW Esports</h1>
      <!--<p class="lead text-white">Nous sommes une structure naissante d'esport, crée en 2022</p>-->
      <p>
        <a href="#apropos" class="btn btn-primary mt-2">À propos »</a>
      </p>
    </div>
  </div>
</section>

<!--À propos-->
<div class="bg-principal">
  <div class="container py-5" id="apropos">
    <h1 class="display-4 fw-bold text-center text-white">Notre Histoire</h1>
    <p class="text-white">Chez BTW² Esports, il nous tient à cœur de donner aux gens le pouvoir de créer une structure leur permettant de trouver un sentiment d'appartenance dans leur vie. Ce que nous voulons, c'est ta faciliter la tâche lorsqu'il s'agit de jouer régulièrement pour la victoire avec ceux que tu aimes. Notre objectif est que tu construises un esprit d'équipe unique aussi bien près de chez toi qu'aux quatre coins du globe dans nos tournois. Originalité, fiabilité, gaieté : telles sont les valeurs qui connectent nos joueurs et nos employés chez BTW Better To Win.</p>
    <h2 class="display-10 fw-bold text-end text-white">Prologue</h2>
    <p class="text-white text-end">BTW² (By The Way, Better To Win) a été créé pour répondre à une problématique importante : Comment rejoindre la scène esport avec ses amis à travers le monde ? Depuis qu'ils ont choisi leur thème de site à effectuer pour leur BTS, les fondatrices Marie Horylinn et Thiméa Nysuna ont partagé l'amour des jeux vidéos et de la scène compétitive, appréciant les amitiés et les liens qui se formaient lors des tournois. À l'époque, tous les outils conçus pour cela étaient lents, instables, complexes et non ouvert au grand public. Marie et Thiméa savaient qu'elles pourraient élaborer un meilleur service qui encouragerait la création d'équipe, aiderait à créer de nouveaux compétiteurs, et recréerait le sentiment de convivialité propre aux jeux.</p>
    <h2 class="display-10 fw-bold text-white">Chapitre I</h2>
    <p class="text-white">Ainsi, en 2022, grâce à Thiméa et Marie, BTW² a vu le jour. À travers la classe de BTS SIO SLAM du lycée Edouard Gand, les gens l'ont adoré (ils n'ont de toute façon pas le choix, la ligne est déjà écrite !). BTW² permet d'entretenir une véritable communication avec son équipe, un véritable esprit d'entaide et de cohésion, bien au-delà de ce qui est faisable chaque jours. Les joueurs étaient tous prêt à gravir de nouveaux échelons. Participerà des tournois était devenu plus facile. La technologie était complexe, mais l'objectif était simple : faire de BTW² un foyer accueillant, confortable et simple pour pouvoir intégrer la scène compétitives de nos jeux favoris. En quelques secondes, BTW² a pris de l'ampleur et, de par le monde, on a vu apparaître des personnes dévouées qui aimaient notre produit.</p>
    <h2 class="display-10 fw-bold text-white">Chapitre II...</h2>
    <p class="text-white">Ecrivez la suite avec nous !</p>
  </div>
</div>


<!--Nos Jeux-->
<div class="bg-secondaire">
  <div class="container py-5" id="nosjeux">
    <h1 class="display-5 fw-bold text-white text-center mb-5">Nos Jeux</h1>

    <!-- Button trigger modal -->
    <?php
    /*
        * On utilise les Session pour savoir si on est admin ou non, pour savoir si on doit afficher les boutons Ajouter & Supprimer jeu !
        */
    if (isset($_SESSION['isAdmin']) && $_SESSION["isAdmin"] == true) {
    ?>
      <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
        Ajouter un jeu
      </button>
      <?php include_once "vues/V_saisieJeux.php"; ?>
      <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#deleteGameBackdrop">
        Supprimer un jeu
      </button>
      <?php include_once "vues/V_supprimerJeux.php"; ?>
    <?php
    }
    ?>

    <?php
    if (is_null($this->data['lesJeux'])) {
      echo '<h2 class="display-5 fw-bold text-white text-center pt-5">Aucun jeu n\'a été enregistré</h2>';
    } else {

      $nb = 0;
    ?>
      <div class="row row-cols-1 row-cols-md-3 g-4 pt-5">
        <?php
        foreach ($this->data['lesJeux'] as $unJeu) {
        ?>
          <div class="col">
            <div class="card h-100">
              <?php echo '<img src="assets/img/games/' . $unJeu->GetImage() . '" class="card-img-top" alt="...">'; ?>
              <div class="card-body">
                <h5 class="card-title text-center text-decoration-underline txtlogo"><?= $unJeu->GetNom(); ?></h5>
                <p class="card-text"><?= $unJeu->GetPetiteDesc(); ?></p>
              </div>
              <div class="card-footer">
                <small class="text-muted"><?php echo '<a href="index.php?page=jeux&id=' . $unJeu->GetId() . '&acronym=' . $unJeu->GetAcronyme() . '">Rejoindre...</a>' ?></small>
              </div>
            </div>
          </div>

      <?php
        }
      }
      ?>
      </div>
  </div>
</div>

<!--Actualité-->
<div class="bg-principal">
  <div class="container pt-5" id="actus">
    <h1 class="display-5 fw-bold text-white text-center mb-5">Les dernières actus</h1>
    <div class="row" data-masonry="{&quot;percentPosition&quot;: true }" style="position: relative; height: 774px;">

      <div class="col-sm-6 col-lg-4 mb-4" style="position: absolute; left: 0%; top: 0px;">
        <div class="card">
          <img class="bd-placeholder-img card-img-top" alt="..." src="assets/img/lolbanner.jpg">
          <div class="card-body">
            <h5 class="card-title">BTW² Esport étend à nouveau son catalogue !</h5>
            <p class="card-text">Après avoir créé des tournois pour Clash Royale et Valorant, nous vous présentons notre nouveau partenaire, League Of Legends !</p>
            <p class="card-text"><small class="text-muted"><cite title="Source Title">Admin</cite>, le 22 Mars 2022</small></p>
          </div>
        </div>
      </div>

      <div class="col-sm-6 col-lg-4 mb-4" style="position: absolute; left: 33.3333%; top: 0px;">
        <div class="card p-3">
          <figure class="p-3 mb-0">
            <blockquote class="blockquote">
              <p>Un nouveau petit partenaire sera annoncé le 22/03, avez-vous une idée de qui est-ce ???</p>
            </blockquote>
            <figcaption class="blockquote-footer mb-0 text-muted">
              <cite title="Source Title">Admin</cite>, le 19 Mars 2022
            </figcaption>
          </figure>
        </div>
      </div>

      <div class="col-sm-6 col-lg-4 mb-4" style="position: absolute; left: 66.6667%; top: 0px;">
        <div class="card">
          <img class="bd-placeholder-img card-img-top" alt="..." src="assets/img/valobanner.jpg">
          <div class="card-body">
            <h5 class="card-title">Nous accueillons un nouveau jeu, Valorant !</h5>
            <p class="card-text">Valorant est un jeu de type FPS compétitif encore en plein essort, il était naturel de l'ajouter à notre collection.<br />Restez informé pour les dates de tournois !</p>
            <p class="card-text"><small class="text-muted"><cite title="Source Title">Admin</cite>, le 17 Mars 2022</small></p>
          </div>
        </div>
      </div>

      <div class="col-sm-6 col-lg-4 mb-4" style="position: absolute; left: 33.3333%; top: 201px;">
        <div class="card bg-primary text-white text-center p-3">
          <figure class="mb-0">
            <blockquote class="blockquote">
              <p>BTW².</p>
              <p>En fait, mieux vaut gagner.</p>
            </blockquote>
            <figcaption class="blockquote-footer mb-0 text-white">
              <cite title="Source Title">Marie & Thiméa</cite>
            </figcaption>
          </figure>
        </div>
      </div>

      <div class="col-sm-6 col-lg-4 mb-4" style="position: absolute; left: 33.3333%; top: 370px;">
        <div class="card text-center">
          <div class="card-body">
            <h5 class="card-title">Un petit vent frais</h5>
            <p class="card-text">Nos réseaux sociaux ont été créés, n'hésitez pas à nous suivre <a href="#reseaux">ici</a>!.</p>
            <p class="card-text"><small class="text-muted"><cite title="Source Title">Admin</cite>, le 16 Mars 2022</small></p>
          </div>
        </div>
      </div>

      <div class="col-sm-6 col-lg-4 mb-4" style="position: absolute; left: 66.6667%; top: 402px;">
        <div class="card">
          <img src="assets/img/Logo.png" alt="Notre Logo" class="bd-placeholder-img card-img" width="100%" height="100%" />
        </div>
      </div>

      <div class="col-sm-6 col-lg-4 mb-4" style="position: absolute; left: 0%; top: 410px;">
        <div class="card p-3 text-end">
          <figure class="mb-0">
            <blockquote class="blockquote">
              <p>La patience est une vertue.</p>
            </blockquote>
            <figcaption class="blockquote-footer mb-0 text-muted">
              <cite title="Source Title">Admin</cite>, le 14 Mars 2022
            </figcaption>
          </figure>
        </div>
      </div>

      <div class="col-sm-6 col-lg-4 mb-4" style="position: absolute; left: 33.3333%; top: 548px;">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">C'est l'ouverture !</h5>
            <p class="card-text">Nous sommes heureuses de pouvoir vous présenter notre plateforme d'Esports, nous avons longuement travaillé sur cette dernière, et espérons qu'elle saura satisfaire vos attentes !<br />À l'heure actuelle nous vous proposons uniquement des tournois Clash Royale, mais cela va s'améliorer !<br />Notre logo n'est-il pas mignon ?</p>
            <figcaption class="blockquote-footer mb-0 text-muted">
              <p class="card-text"><small class="text-muted"><cite title="Source Title">Marie & Thiméa</cite>, le 15 Mars 2022</small></p>
            </figcaption>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>