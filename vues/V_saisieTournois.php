      <!-- Modal -->
      <?php
      /*
        * On utilise les Modal de bootstrap pour faire notre page.
        */
      ?>
      <div class="modal fade" id="staticBackdropCreeTournoi" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropCreeTournoiLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="staticBackdropLabel">Créer un tournoi</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="index.php?page=ajouttournois" method="POST" enctype="multipart/form-data">
              <div class="modal-body">
                <div class="container-fluid">
                  <div class="mb-3">
                    <label for="nomTournoi" class="form-label">Nom:</label>
                    <input type="text" name="nomTournoi" class="form-control" id="inputnomTournoi" aria-describedby="nomTournoiHelp" minlength="3" maxlength="250" required>
                    <div class="form-text" id="nomTournoiHelp"></div>
                  </div>
                  <div class="mb-3">
                    <label for="jeuTournoi" class="form-label">Jeu:</label>
                    <select name="jeuTournoi" class="form-select" id="inputjeuTournoi" aria-describedby="jeuTournoiHelp" required>
                      <option selected>-- Séléctionner --</option>
                      <?php foreach ($this->data['lesJeuxEnregister'] as $jeux) { ?>
                        <option value="<?= $jeux->GetId(); ?>"><?= $jeux->GetNom(); ?></option>
                      <?php } ?>
                    </select>
                    <div class="form-text" id="jeuTournoiHelp"></div>
                  </div>
                  <div class="mb-3">
                    <label for="nbParticipantsTournoi" class="form-label">Nombre de participants:</label>
                    <select name="nbParticipantsTournoi" class="form-select" id="inputnbParticipantsTournoi" aria-describedby="nbnbParticipantsTournoiHelp" required>
                      <option selected>-- Séléctionner --</option>
                      <option value="8">8</option>
                      <option value="16">16</option>
                      <option value="32">32</option>
                    </select>
                    <div class="form-text" id="nbParticipantsHelp"></div>
                  </div>
                  <div class="mb-3">
                    <label for="dateHeureTournoi" class="form-label">Date et heure:</label>
                    <?php $jour = date("Y-m-d");
                    $maDate = strtotime($jour . "+ 1 days"); ?>
                    <input type="date" name="dateHeureTournoi" class="form-control" id="inputdateHeureTournoi" aria-describedby="dateHeureTournoiHelp" value="<?= date("Y-m-d", $maDate) ?>" min="<?= date("Y-m-d", $maDate) ?>" required>
                    <div class="form-text" id="dateHeureTournoiHelp"></div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Créer le tournoi</button>
                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Annuler</button>
              </div>
            </form>
          </div>
        </div>
      </div>