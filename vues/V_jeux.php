<?php
//Si on a aucun jeu en paramètre de data (si on ouvre la page sans avoir d'id, ou un mauvais), alors erreur 404 !
if (is_null($this->data['leJeu'])) {
  require_once("vues/V_error404.php");
} else {
?>
  <!--Bannière début-->
  <div class="bg-principal">
    <?php
    echo "<section class='py-5 text-center container bg-image-game' style='background: linear-gradient(to bottom, rgba(255,255,0,0.5), rgba(0,0,255,0.5)), url(\"assets/img/games/" . $this->data['leJeu']->GetImage() . "\")';>";
    ?>
    <div class="row py-lg-5">
      <div class="col-lg-6 col-md-8 mx-auto">
        <h1 class="fw-light text-white mt-5 pt-5 fw-bold"><?= $this->data['leJeu']->GetNom(); ?></h1>
        <p>
          <a href="#apropos" class="btn btn-primary mt-2">À propos »</a>
        </p>
      </div>
    </div>
    </section>

    <!--À propos-->
    <div class="container py-5" id="apropos">
      <h1 class="display-4 fw-bold text-start text-white">Description</h1>
      <p class="text-white"><?= $this->data['leJeu']->GetDesc(); ?></p>
      <p class="text-white">Acronyme: <?= $this->data['leJeu']->GetAcronyme(); ?></p>
      <p class="text-white">Nombre de participants par équipe: <?= $this->data['leJeu']->GetNbParticipantsParEquipe() - 1; ?></p>
      <a class="btn btn-info mt-5" href="index.php?page=listetournois#nostournois<?=$this->data['leJeu']->GetNom();?>">Trouver un tournoi</a>
    </div>
  </div>

<?php
}
?>