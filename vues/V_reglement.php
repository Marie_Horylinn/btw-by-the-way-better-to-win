<!--À propos-->
<div class="bg-principal">
    <div class="container py-5" id="apropos">
        <h1 class="display-4 fw-bold text-start text-white">Règlement</h1>
        <p class="text-white pt-5">
            1 – Tous les membres sont égaux au sein de la structure, et ce, quel que soit leur âge, leur nationalité, leur orientation sexuelle ou encore leur niveau de jeu.
        </p>
        <p class="text-white">
            2 – Le respect est fondamental pour préserver un état d’esprit communautaire et jovial au sein de la structure. Exemple: Diffamation, racisme, sexisme, harcèlement, propos ayant pour but d’humilier ou de rabaisser un autre membre ou possible futur membre ne sera toléré cela vaudra une radiation immédiate
        </p>
        <p class="text-white">
            3 – La politesse est également de rigueur. Que ce soit en jeu, sur le teamspeak ou sur le forum, un « bonjour », « s’il vous plait », et « merci » n’ont jamais fait de mal à quiconque et rendent les échanges plus agréables.
        </p>
        <p class="text-white">
            4 – Chaque fin de mois, une réunion de team a lieu. Votre présence est grandement souhaitée. Vous serez mis au courant des objectifs accomplis durant ce mois, des prochains objectifs et de l’actualité des autres sections de jeux actifs.
        </p>
        <p class="text-white">
            5 – Le fair-play est une règle essentielle dans la pratique des jeux. N’insultez pas vos coéquipiers, n’ayez pas des coups de sang impromptus, félicitez vos adversaires dans la victoire comme dans la défaite. Perdre fait également partie des règles du jeu. Partez du principe que même Hercule s’est fait avoir.
        </p>
        <p class="text-white">
            6 – Chaque membre peut apporter sa pierre à l’édifice à condition que le projet présenté soit parfaitement étudié et clair. Des arguments fondés et des exemples concis mettront davantage en avant votre projet. L’investissement est un devoir important au sein de la structure, à condition que celui-ci soit destiné à l’ensemble des membres et réfléchi.
        </p>
        <p class="text-white">
            7 – Toute tentative de triche, quelle qu’elle soit, est prohibée sur la plateforme de jeux de « CrystaL-Gaming » et sera sanctionnée. Sachez qu’un bon joueur acquiert une expérience solide avec le temps et non par la duperie.
        </p>
        <p class="text-white">
            8 – Vous représentez une communauté. Les comportements égoïstes sont également à proscrire.
        </p>
        <p class="text-white">
            9 – Pour intégrer notre structure, vous devez obligatoirement passer par une candidature et un entretien teamspeak avec un responsable (Fondateur/Staff/Responsable section). Si votre candidature est validée, vous serez, durant un mois, en « Test ». À la fin de ce délai, si le résultat est positif, vous serez Membre de l’association CrystaL-Gaming.
        </p>
        <p class="text-white">
            10 – Une cotisation n’est pas obligatoire. Mais n’hésitez pas à faire des dons, cela servira à améliorer la structure (Graphiste, Maillot, Site, Chaîne twitch, Déplacement).
        </p>
        <p class="text-white">
            11 – CrystaL-Gaming ne tolérera pas le multi-team/Asso/structure, il est de votre seul ressort d’assurer vos engagements envers la structure. Faire partie de deux structures à la fois ne sera pas possible chez nous.
        </p>
        <p class="text-white">
            12 – Les Cotisation ne sont pas remboursable.
        </p>

    </div>
</div>