<div class="bg-principal">
    <div class="container py-5">
        <p class="fw-bold text-white text-center mb-5">
            Nom: <?= $this->data["leTournois"]->GetNom(); ?><br />
            Id: <?= $this->data["leTournois"]->GetId(); ?><br />
            Nombre d'équipes: <?= $this->data["leTournois"]->GetNbEquipe(); ?><br />
            Date: <?= $this->data["leTournois"]->GetDateHeure(); ?>
        </p>
        <?php if (!empty($this->data['leHistorique'])) { ?>

            <table class="table text-white">
                <thead>
                    <tr>
                        <th scope="col">Classement</th>
                        <th scope="col">Equipe</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 0;
                    foreach ($this->data["leHistorique"] as $historique) { ?>
                        <tr class="text-white">
                            <th scope="row"><?= $historique->GetPlace(); ?></th>
                            <td><?= $this->data["lesEquipes"][$i]->GetNom(); ?></td>
                            <?php $i++ ?>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        <?php } else {
            echo "<br/><p class='text-white text-center'>Les résultats pour ce tournoi n'ont pas encore été enregistrés.</p>";
        } ?>
    </div>
</div>