<div class="position-fixed bottom-0 end-0 p-3" style="z-index: 11">
  <div id="liveMessageToast" class="toast" role="alert" aria-live="assertive" aria-atomic="true">
    <div class="toast-header">
      <?php
      echo "<img src='assets/img/" . $_SESSION["colorleMessage"] . "' class='rounded me-2' style='height:20px; width:20px;' alt='...'>";
      ?>
      <strong class="me-auto"><?= $_SESSION["titreleMessage"] ?></strong>
      <small>Il y a quelques secondes...</small>
      <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
    </div>
    <div class="toast-body">
      <?= $_SESSION["leMessage"] ?>
    </div>
  </div>
</div>

<script>
  var toastLeMessage = document.getElementById('liveMessageToast');
  var toast = new bootstrap.Toast(toastLeMessage);
  toast.show();
</script>

<?php
/*
  * On utilise les Toast de bootstrap pour afficher les messages en bas à droite. Le Script vient aussi de bootstrap
  */
?>