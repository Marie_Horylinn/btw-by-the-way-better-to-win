<div class="bg-principal">
  <?php
  $controleur = new c_inscription();
  ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <form class="box py-5" action="index.php?page=ajoutmembre" method="post" id="formInscription" enctype="multipart/form-data">
    <div class="container text-center inscription py-5">
      <p id="titre_inscri">S'inscrire sur Better To Win</p><br />
      Nom : </br>
      <input type='text' name='nom' placeholder="Votre nom" required />
      <br />
      Prénom : </br>
      <input type='text' name='prenom' placeholder="Votre prénom" required />
      <br />
      Mail : </br>
      <input type='text' name='mail' placeholder="Votre mail" required />
      <br />
      Tel : </br>
      <input type='text' name='tel' placeholder="Votre numéro de téléphone" required />
      <br />
      Pseudo : </br>
      <input type='text' name='pseudo' placeholder="Votre pseudo" required />
      <br />
      Mot de passe : </br>
      <input type='password' name='mdp' placeholder="Votre mot de passe" required /><br /><br />
      Je suis un
      <select name="choix_categ" id="membreType" onchange="changeAction(this.value);" required>
        <?php if (is_null($this->data['lesEquipes'][0])) {
          echo '<option value="2">Gérant</option>';
        } else {
          echo '<option value="3">Joueur</option>';
          echo '<option value="2">Gérant</option>';
        } ?>
      </select><br /><br />
      <div id="choixequipenom"> Je joue dans l'équipe </div>
      <select name="choix_equipe" id="choixequipe" required>
        <?php foreach ($this->data['lesEquipes'] as $uneEquipe) {
        ?><option value="<?php echo $uneEquipe->GetId(); ?>"><?php echo $uneEquipe->GetNom(); ?></option><?php
                                                                                                        }
                                                                                                          ?>
      </select><br /><br />
      <div id="uwu"></div>
      <input type='submit' value='inscription' class="btn btn-primary" />
    </div>
  </form>
</div>

<script>
  function changeAction(val) {
    var ddl = document.getElementById("membreType");
    var selectedValue = ddl.options[ddl.selectedIndex].value;
    if (selectedValue == "2") {
      document.getElementById("formInscription").action = "index.php?page=ajoutgerant";
      document.getElementById("uwu").innerHTML = "<p id=\"titre_equipe\">Créer mon équipe</p><br />" +
        "Nom d'équipe: </br>" +
        "<input type='text' name='nom_equipe' placeholder=\"Nom d'équipe\" required/>" +
        "<br />" +
        "Description : </br>" +
        "<input type='text' name='description_equipe' placeholder=\"Description d'équipe\" required/>" +
        "<br />" +
        "<label for=\"profileImageJeu\" class=\"form-label\">Image:</label><br/>" +
        "<input id=\"imageUpload\" name=\"profileImageJeu\" type=\"file\" accept=\"image/jpeg\" placeholder=\"Photo\" required=\"\" capture required><br/> " +
        "<image id=\"profileImage\" src=\"\" class=\"my-3 img-thumbnail profileImg\"/> " +
        "<br /><br />";
      document.getElementById("choixequipe").hidden = true;
      document.getElementById("choixequipenom").hidden = true;
      document.getElementById("choixequipe").required = false;
    } else {
      document.getElementById("formInscription").action = "index.php?page=ajoutmembre";
      document.getElementById("uwu").innerHTML = "";
      document.getElementById("choixequipe").hidden = false;
      document.getElementById("choixequipenom").hidden = false;
      document.getElementById("choixequipe").required = true;
    }
  }
</script>