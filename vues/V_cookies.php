<!-- Modal -->
<?php
/*
        * On utilise les Modal de bootstrap pour faire notre page.
        */
?>
<div class="modal fade" id="staticBackdropCookies" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Conditions générales d'utilisation</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <p>
            Les présentes conditions générales d'utilisation (dites « CGU ») ont pour objet l'encadrement juridique
            <br />des modalités de mise à disposition du site et des services par _______________ et de définir les
            <br />conditions d’accès et d’utilisation des services par « l'Utilisateur ».
            <br />Les présentes CGU sont accessibles sur le site à la rubrique «CGU».
          </p>
          <br /><br />
          <p>
            <br />Toute inscription ou utilisation du site implique l'acceptation sans aucune réserve ni restriction des
            <br />présentes CGU par l’utilisateur. Lors de l'inscription sur le site via le Formulaire d’inscription, chaque
            <br />utilisateur accepte expressément les présentes CGU en cochant la case précédant le texte suivant : «
            <br />Je reconnais avoir lu et compris les CGU et je les accepte ».
            <br />En cas de non-acceptation des CGU stipulées dans le présent contrat, l'Utilisateur se doit de renoncer
            <br />à l'accès des services proposés par le site.
            <br />bettertowin.fr se réserve le droit de modifier unilatéralement et à tout moment le contenu des
            <br />présentes CGU.
          </p><br /><br /><br />
          <br /><br />
          <h1>Article 1 : Les mentions légales</h1>
          <p>
            <br />L’édition et la direction de la publication du site bettertowin.fr est assurée par Horylinn Marie, domicilié
            <br />Adresse de BTW.
            <br />Numéro de téléphone est 0101010101
            <br />Adresse e-mail mariehory@btw.fr.
          </p>
          <br /><br />
          <p>
            <br />L'hébergeur du site bettertowin.fr est la société Lycée Edouard Gand, dont le siège social est situé
            <br />au /o/, avec le numéro de téléphone : 0202020202.
          </p>
          <br /><br />
          <h1>Article 2 : Accès au site</h1>
          <p>
            <br />Le site bettertowin.fr permet à l'Utilisateur un accès gratuit aux services suivants :
            <br />Le site internet propose les services suivants :
            <br />Inscriptions à des tournois
            <br />Le site est accessible gratuitement en tout lieu à tout Utilisateur ayant un accès à Internet. Tous les
            <br />frais supportés par l'Utilisateur pour accéder au service (matériel informatique, logiciels, connexion
            <br />Internet, etc.) sont à sa charge.<br />

            <br />L’Utilisateur non membre n'a pas accès aux services réservés. Pour cela, il doit s’inscrire en
            <br />remplissant le formulaire. En acceptant de s’inscrire aux services réservés, l’Utilisateur membre
            <br />s’engage à fournir des informations sincères et exactes concernant son état civil et ses coordonnées,
            <br />notamment son adresse email.<br />

            <br />Pour accéder aux services, l’Utilisateur doit ensuite s'identifier à l'aide de son identifiant et de son mot
            <br />de passe qui lui seront communiqués après son inscription.
            <br />Tout Utilisateur membre régulièrement inscrit pourra également solliciter sa désinscription en se
            <br />rendant à la page dédiée sur son espace personnel. Celle-ci sera effective dans un délai raisonnable.
            <br />Tout événement dû à un cas de force majeure ayant pour conséquence un dysfonctionnement du site
            <br />ou serveur et sous réserve de toute interruption ou modification en cas de maintenance, n'engage pas
            <br />la responsabilité de bettertowin.fr. Dans ces cas, l’Utilisateur accepte ainsi ne pas tenir rigueur à
            <br />l’éditeur de toute interruption ou suspension de service, même sans préavis.
            <br />L'Utilisateur a la possibilité de contacter le site par messagerie électronique à l’adresse email de
            <br />l’éditeur communiqué à l’ARTICLE 1.
          </p><br /><br />
          <h1>Article 3 : Collecte des données</h1>
          <p>
            <br />Le site assure à l'Utilisateur une collecte et un traitement d'informations personnelles dans le respect
            <br />de la vie privée conformément à la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers
            <br />et aux libertés. Le site est déclaré à la CNIL sous le numéro 4646464913264.
            <br />En vertu de la loi Informatique et Libertés, en date du 6 janvier 1978, l'Utilisateur dispose d'un droit
            <br />d'accès, de rectification, de suppression et d'opposition de ses données personnelles. L'Utilisateur
            <br />exerce ce droit :
            <br />· via son espace personnel ;
          </p><br /><br />
          <h1>Article 4 : Propriété intellectuelle</h1>
          <p>
            <br />Les marques, logos, signes ainsi que tous les contenus du site (textes, images, son…) font l'objet
            <br />d'une protection par le Code de la propriété intellectuelle et plus particulièrement par le droit d'auteur.<br /><br />

            <br />La marque BTW est une marque déposée par BetterToWin Esports.Toute représentation et/ou
            <br />reproduction et/ou exploitation partielle ou totale de cette marque, de quelque nature que ce soit, est
            <br />totalement prohibée.<br /><br />

            <br />L'Utilisateur doit solliciter l'autorisation préalable du site pour toute reproduction, publication, copie des
            <br />différents contenus. Il s'engage à une utilisation des contenus du site dans un cadre strictement privé,
            <br />toute utilisation à des fins commerciales et publicitaires est strictement interdite.
            <br />Toute représentation totale ou partielle de ce site par quelque procédé que ce soit, sans l’autorisat <br />expresse de l’exploitant du site Internet constituerait une contrefaçon sanctionnée par l’article L 335-2
            <br />et suivants du Code de la propriété intellectuelle.
            <br />Il est rappelé conformément à l’article L122-5 du Code de propriété intellectuelle que l’Utilisateur qui
            <br />reproduit, copie ou publie le contenu protégé doit citer l’auteur et sa source.
          </p><br /><br />
          <h1>Article 5 : Responsabilité</h1>
          <p>
            <br />Les sources des informations diffusées sur le site bettertowin.fr sont réputées fiables mais le site ne
            <br />garantit pas qu’il soit exempt de défauts, d’erreurs ou d’omissions.<br />

            <br />Les informations communiquées sont présentées à titre indicatif et général sans valeur contractuelle.
            <br />Malgré des mises à jour régulières, le site bettertowin.fr ne peut être tenu responsable de la
            <br />modification des dispositions administratives et juridiques survenant après la publication. De même, le
            <br />site ne peut être tenue responsable de l’utilisation et de l’interprétation de l’information contenue dans
            <br />ce site.
            <br />L'Utilisateur s'assure de garder son mot de passe secret. Toute divulgation du mot de passe, quelle
            <br />que soit sa forme, est interdite. Il assume les risques liés à l'utilisation de son identifiant et mot de
            <br />passe. Le site décline toute responsabilité.
            <br />Le site bettertowin.fr ne peut être tenu pour responsable d’éventuels virus qui pourraient infecter
            <br />l’ordinateur ou tout matériel informatique de l’Internaute, suite à une utilisation, à l’accès, ou au
            <br />téléchargement provenant de ce site.
            <br />La responsabilité du site ne peut être engagée en cas de force majeure ou du fait imprévisible et
            <br />insurmontable d'un tiers.
          </p><br /><br />
          <h1>Article 6 : Liens hypertextes</h1>
          <p>
            <br />Des liens hypertextes peuvent être présents sur le site. L’Utilisateur est informé qu’en cliquant sur ces
            <br />liens, il sortira du site bettertowin.fr. Ce dernier n’a pas de contrôle sur les pages web sur lesquelles
            <br />aboutissent ces liens et ne saurait, en aucun cas, être responsable de leur contenu.
          </p><br /><br />
          <h1>Article 7 : Cookies</h1>
          <p>
            <br />L’Utilisateur est informé que lors de ses visites sur le site, un cookie peut s’installer automatiquement
            <br />sur son logiciel de navigation.
            <br />Les cookies sont de petits fichiers stockés temporairement sur le disque dur de l’ordinateur de
            <br />l’Utilisateur par votre navigateur et qui sont nécessaires à l’utilisation du site bettertowin.fr. Les
            <br />cookies ne contiennent pas d’information personnelle et ne peuvent pas être utilisés pour identifier
            <br />quelqu’un. Un cookie contient un identifiant unique, généré aléatoirement et donc anonyme. Certains
            <br />cookies expirent à la fin de la visite de l’Utilisateur, d’autres restent.
            <br />L’information contenue dans les cookies est utilisée pour améliorer le site bettertowin.fr.
            <br />En naviguant sur le site, L’Utilisateur les accepte.
            <br />L’Utilisateur doit toutefois donner son consentement quant à l’utilisation de certains cookies.
            <br />A défaut d’acceptation, l’Utilisateur est informé que certaines fonctionnalités ou pages risquent de lui
            <br />être refusées.
            <br />L’Utilisateur pourra désactiver ces cookies par l’intermédiaire des paramètres figurant au sein de son
            <br />logiciel de navigation.
          </p><br /><br />
          <h1>Article 8 : Droit applicable et juridiction compétente</h1>
          <p>
            <br />La législation française s'applique au présent contrat. En cas d'absence de résolution amiable d'un
            <br />litige né entre les parties, les tribunaux français seront seuls compétents pour en connaître.
            <br />Pour toute question relative à l’application des présentes CGU, vous pouvez joindre l’éditeur aux
            <br />coordonnées inscrites à l’ARTICLE 1.
          </p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Quitter</button>
      </div>
    </div>
  </div>
</div>