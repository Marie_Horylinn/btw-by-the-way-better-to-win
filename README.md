# BTW By The Way Better To Win

Projet BTW, avec Thiméa

# Ce qu'il est possible de faire sur le site.

- Créer un Compte (Membre ou Gérant) ou Se connecter. Il n'et pas possible de se créer un compte Admin.
- Créer/Supprimer des Tournois
- Rejoindre des tournois en tant que Gérant.
- Accéder à l'historique de nos tournois et des tournois d'équipe + Les tournois à laquelle notre équipe est inscrite actuellement
- Créer/Supprimer des jeux (Il se peut que ça bug au niveau de l'affichage des tournois, si les jeux sont supprimés.)
- Supprimer des comptes de Membres/Gérants (Pas d'Admin) -> Cela peut bug au niveau des équipes, elles peuvent se retrouver sans gérant
- 1 Seul gérant par équipe, pas moyen d'en avoir d'autres
- Accéder à nos réseaux, Actualités, Règles,
- Bug qui faut qu'on peut accéder aux conditions d'utilisation uniquement si on est connecté.
- Lorsqu'on clique sur un tournoi "Vois les résultats" ils vont se générer dans la BDD aléatoirement s'ils ne sont pas déjà générés
- Un clic sur nos logos Footer de BTW vous aménère en haut de page.
- Un Clic sur le logo Navbar BTW amène à l'accueil.
- Les participations aux tournois échouent si on dépasse le nombre d'équipe prévu. (Mais le bouton propose toujours de s'inscrire).
- Modifier nos informations de compte.
- Gestion des images de jeux/membres/equipes
- À chaque action, un petit message s'affiche en bas à droite nous disant si c'est réussi ou non, et nous redirige.
- Si une page n'est pas censé être ouverte sans arguments Post/Get/Session, alors elle affichera Erreur 404. (Sauf si oubli.)

- La base de donnée "BTW.sql" contient les tables et les données, de la base "btw".

# Connexions au Site.

Plusieurs comptes ont été créés, avec différentes permissions, les voicis:
(Tous les comptes utilisent '0000' comme mot de passe.)

Voici les pseudonymes.

Admin:
> Horylinn (Admin du site + team Vitality)

> Nysuna (Admin du site + team Vitality)


Gérants d'équipes:
> Gotaga (Team Vitality)

> Kameto (Team Kcorp)

> Wakz (Team Solary)


Joueurs:
> Skeans (Team Vitality)

> Mephisto (Team Vitality)

> Cabochard (Team Kcorp)

> Saken (Team Kcorp)

> Wao (Team Kcorp)

> Djoko (Team Solary)


# Les permissions:

Les permissions les plus hautes héritent de celles en dessous.

Admin:
> Créer/Supprimer des jeux, des tournois

> Supprimer des membres dans le Panel Admin

Gérants:
> Créer son équipe

> Rejoindre des tournois (Y inscrire son équipe.)


Membres:
> Modifier leurs informations

> Voir les tournois rejoins dans leur équipes

> Voir l'historique des tournois de leur équipes

> Voir leur propre historique de tournois


# Point négatif: (Ils sont là par manque de temps, et certainement de motivation à continuer, c'est long le MVC !)

> Un Admin ne peut pas modifier un membre. (Suppression uniquement)

> Un Admin ne peut pas modifier une équipe.

> Un gérant ne peut pas modifier une équipe. Il ne peut pas non plus changer d'équipe.

> Il n'existe pas de page pour rentrer les scores d'un tournoi. Ils sont générés aléatoirement la première fois qu'ils sont regardés.

> Il n'y a pas moyen de se désinscrire d'un tournoi. Il n'y a pas moyen de modifier un tournoi. (Sa date par exemple)

> Lorsqu'on modifie les infos utilisateurs, on est obligé de modifier sa photo et son mot de passe.

# Point positif:
> C'était amusant, en plus il est joli le site.

## Ajouter tes fichiers/dossiers

- Ouvrir un CMD dans le dossier principal.

```
git status
```
Si le Git Status envoi des changements nécessaires
```
git add --all
git commit -m "Texte de la modification, qu'as-tu ajouté/supprimé"
git push
```

OU

- Dans le dossier, faire un clic droit et "Git Gui Here"
Cela va ouvrir un menu, à l'intérieur du quel tu peux voir :
> Current Branch: .... (Tu dois avoir affiché "main")

> Les "Unstaged Changes", donc les changements qui sont sur tes fichies mais pas en ligne. 

> Les "Staged Changes", les changements qui sont prêts à être mis en ligne.

Pour mettre une modification dans "Staged Changes", il faut que tu cliques sur cette dernières qui se trouve dans "Unstaged Change", puis tu utilises le bouton "Stage Changed". (Tu peux en séléctionner plusieurs avec le CTRL + Clic)
Ensuite, dans la case "Commit message" tu mets en texte français ce que tu as fais avec cette modification, ce qu'elle a ajouté/supprimé.
Puis tu appuies sur "Commit", puis sur "Push".

Si tes modifications n'apparaissent pas dans "Unstaged Change", utilise le bouton "Rescan".

Voilà, ça a été envoyé sur le serveur.

## Récupérer les fichiers présents sur le GIT

Il faut utiliser le CMD dans le dossier, pour faire la commande
```
git status  (pour afficher s'il y a des changements)
git pull    (pour récupérer les changements.)
```
Attention, parfois il faut mieux envoyer sur le serveur avant de Pull, dépendant du Status !

## Inviter des membres

- [ ] [Inviter des membres sur le projet](https://docs.gitlab.com/ee/user/project/members/)