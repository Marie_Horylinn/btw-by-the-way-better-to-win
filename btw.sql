-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 07 avr. 2022 à 08:44
-- Version du serveur :  10.4.14-MariaDB
-- Version de PHP : 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `btw`
--

-- --------------------------------------------------------

--
-- Structure de la table `equipe`
--

CREATE TABLE `equipe` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `equipe`
--

INSERT INTO `equipe` (`id`, `nom`, `description`, `image`) VALUES
(2, 'Vitality', 'La Team Vitality, ou simplement Vitality, est un club d\'esport français fondé en 2013', 'vitality.jpg'),
(3, 'Karmin Corp', 'a Karmine Corp (surnommée KCorp) est une structure française centrée autour d\'une équipe e-sport immatriculée à Tours1 et basée à Paris2. Elle est formée le 30 mars 2020 sous le nom de Kameto Corp par le duo de streamers francophones Kameto et Kotei3. Le 16 novembre 2020, l\'entrepreneur et rappeur français Prime rejoint la direction de la structure, et l\'équipe prend sa dénomination actuelle4. Une société par actions simplifiée, présidée par Kameto et dirigée par Prime, est créée quelques mois plus tard', '2Kameto_Corp_logo.jpg'),
(4, 'Solary', 'Solary team', '3Solary.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

CREATE TABLE `groupe` (
  `id` tinyint(4) NOT NULL,
  `libelle` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `groupe`
--

INSERT INTO `groupe` (`id`, `libelle`) VALUES
(1, 'admin'),
(2, 'gérant'),
(3, 'membre');

-- --------------------------------------------------------

--
-- Structure de la table `historiquetournois`
--

CREATE TABLE `historiquetournois` (
  `id_membre` int(11) NOT NULL,
  `id_tournois` int(11) NOT NULL,
  `id_equipe` int(11) NOT NULL,
  `place` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `historiquetournois`
--

INSERT INTO `historiquetournois` (`id_membre`, `id_tournois`, `id_equipe`, `place`) VALUES
(1, 1, 2, 3),
(2, 1, 2, 3),
(3, 1, 2, 3),
(4, 1, 2, 3),
(5, 1, 3, 2),
(6, 1, 3, 2),
(7, 1, 3, 2),
(8, 1, 3, 2),
(9, 1, 4, 1),
(10, 1, 4, 1),
(11, 1, 2, 3);

-- --------------------------------------------------------

--
-- Structure de la table `jeux`
--

CREATE TABLE `jeux` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `acronyme` varchar(4) NOT NULL,
  `petite_desc` text NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `nb_participants` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `jeux`
--

INSERT INTO `jeux` (`id`, `nom`, `acronyme`, `petite_desc`, `description`, `image`, `nb_participants`) VALUES
(1, 'League Of Legends', 'LoL', 'League of legends est un jeu MOBA en 5v5', 'League of Legends is one of the world\'s most popular video games, developed by Riot Games. It features a team-based competitive game mode based on strategy and outplaying opponents. Players work with their team to break the enemy Nexus before the enemy team breaks theirs.', '2lolbanner.jpg', 6),
(2, 'Valorant', 'Valo', 'Valorant est un jeu FPS en 5v5', 'Valorant is a tactical shooting game involving two teams with 5 players in each team. Every player can sign in and play remotely from anywhere in the world. Every game has 25 rounds and the team that wins 13 of them first wins the game. Players can choose their in-game characters called agents at the start of the game.', '2valobanner.jpg', 6),
(3, 'Clash Royale', 'CR', 'Jeu de type MOBA Tower Défense en 2v2', 'Clash Royale est un jeu vidéo développé et édité par Supercell, sorti en France le 2 mars 2016 sur iOS et Android. Il s\'agit d\'un jeu se basant sur des duels multijoueurs temps réel et mêlant des éléments de tower defense et MOBA, avec un système de cartes à collectionner.', '3crbanner.jpg', 3);

-- --------------------------------------------------------

--
-- Structure de la table `membre`
--

CREATE TABLE `membre` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `prenom` varchar(100) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `tel` varchar(100) NOT NULL,
  `pseudo` varchar(100) NOT NULL,
  `mdp` varchar(100) NOT NULL,
  `image` varchar(255) NOT NULL,
  `id_categorie` tinyint(4) NOT NULL,
  `id_equipe` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `membre`
--

INSERT INTO `membre` (`id`, `nom`, `prenom`, `mail`, `tel`, `pseudo`, `mdp`, `image`, `id_categorie`, `id_equipe`) VALUES
(1, 'Poirel', 'Marie', 'mariehory@btw.fr', '0606060606', 'Horylinn', '0000', '1holo.jpg', 1, 2),
(2, 'Houssein', 'Corentin', 'correntinhoussein@vitality.fr', '0102030405', 'Gotaga', '4a7d1ed414474e4033ac29ccb8653d9b', 'profilbase.png', 2, 2),
(3, 'Duncan', 'Marquet', 'duncanmarquet@vitality.fr', '0203040506', 'Skeanz', '4a7d1ed414474e4033ac29ccb8653d9b', 'profilbase.png', 3, 2),
(4, 'Legendre', 'Louis-Victor', 'lvlegendre@vitality.fr', '0304050607', 'Mephisto', '4a7d1ed414474e4033ac29ccb8653d9b', 'profilbase.png', 3, 2),
(5, 'Kebir', 'Kamel', 'kamelkebir@kcorp.fr', '0405060708', 'Kameto', '4a7d1ed414474e4033ac29ccb8653d9b', 'profilbase.png', 2, 3),
(6, 'Simon-Meslet', 'Lucas', 'simonlucas@kcorp.fr', '0102030405', 'Cabochard', '4a7d1ed414474e4033ac29ccb8653d9b', 'profilbase.png', 3, 3),
(7, 'Fayard', 'Lucas', 'fayarlucas@kcorp.fr', '0908070603', 'Saken', '4a7d1ed414474e4033ac29ccb8653d9b', 'profilbase.png', 3, 3),
(8, 'Dai', 'Wao', 'waodai@kcorp.fr', '0807060305', 'Wao', '0000', '8cropped-WAO-Icone.jpg', 3, 3),
(9, 'Hugues', 'César', 'huguescesar@solary.fr', '0405060708', 'Wakz', '4a7d1ed414474e4033ac29ccb8653d9b', 'profilbase.png', 2, 4),
(10, 'Guillard', 'Charly', 'guillard@solary.fr', '0506070809', 'Djoko', '4a7d1ed414474e4033ac29ccb8653d9b', 'profilbase.png', 3, 4),
(11, 'Holleville', 'Thiméa', 'thimnysuna@gmail.com', '0707070707', 'Nysuna', '4a7d1ed414474e4033ac29ccb8653d9b', 'profilbase.png', 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `participer`
--

CREATE TABLE `participer` (
  `id_equipe` int(11) NOT NULL,
  `id_tournois` int(11) NOT NULL,
  `id_membre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `participer`
--

INSERT INTO `participer` (`id_equipe`, `id_tournois`, `id_membre`) VALUES
(2, 1, 1),
(2, 1, 2),
(2, 1, 3),
(2, 1, 4),
(2, 1, 11),
(2, 5, 1),
(2, 5, 2),
(2, 5, 3),
(2, 5, 4),
(2, 5, 11),
(2, 8, 1),
(2, 8, 2),
(2, 8, 3),
(2, 8, 4),
(2, 8, 11),
(3, 1, 5),
(3, 1, 6),
(3, 1, 7),
(3, 1, 8),
(3, 3, 5),
(3, 3, 6),
(3, 3, 7),
(3, 3, 8),
(3, 6, 5),
(3, 6, 6),
(3, 6, 7),
(3, 6, 8),
(4, 1, 9),
(4, 1, 10),
(4, 2, 9),
(4, 2, 10),
(4, 6, 9),
(4, 6, 10),
(4, 7, 9),
(4, 7, 10);

-- --------------------------------------------------------

--
-- Structure de la table `tournois`
--

CREATE TABLE `tournois` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `DateHeure` datetime NOT NULL,
  `nbEquipe` int(11) NOT NULL,
  `idJeu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tournois`
--

INSERT INTO `tournois` (`id`, `nom`, `DateHeure`, `nbEquipe`, `idJeu`) VALUES
(1, 'Worlds LoL', '2022-04-06 00:30:29', 32, 1),
(2, 'MSI LoL', '2022-04-14 00:00:00', 16, 1),
(3, 'LCS LoL', '2022-04-21 00:00:00', 8, 1),
(4, 'Valorant Champions Tour', '2022-04-07 00:00:00', 32, 2),
(5, 'Valorant Regional', '2022-04-17 00:00:00', 16, 2),
(6, 'Crown Championship', '2022-04-09 00:00:00', 8, 3),
(7, 'La débandade Royale', '2022-04-15 00:00:00', 32, 3),
(8, 'La Grosse League', '2022-04-28 00:00:00', 32, 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `equipe`
--
ALTER TABLE `equipe`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `groupe`
--
ALTER TABLE `groupe`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `historiquetournois`
--
ALTER TABLE `historiquetournois`
  ADD PRIMARY KEY (`id_membre`,`id_tournois`,`id_equipe`),
  ADD KEY `id_tournois` (`id_tournois`),
  ADD KEY `id_equipe` (`id_equipe`);

--
-- Index pour la table `jeux`
--
ALTER TABLE `jeux`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `membre`
--
ALTER TABLE `membre`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_equipe` (`id_equipe`),
  ADD KEY `id_categorie` (`id_categorie`);

--
-- Index pour la table `participer`
--
ALTER TABLE `participer`
  ADD PRIMARY KEY (`id_equipe`,`id_tournois`,`id_membre`),
  ADD KEY `id_tournois` (`id_tournois`),
  ADD KEY `id_membre` (`id_membre`);

--
-- Index pour la table `tournois`
--
ALTER TABLE `tournois`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idJeu` (`idJeu`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `equipe`
--
ALTER TABLE `equipe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `groupe`
--
ALTER TABLE `groupe`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `jeux`
--
ALTER TABLE `jeux`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `membre`
--
ALTER TABLE `membre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `tournois`
--
ALTER TABLE `tournois`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `historiquetournois`
--
ALTER TABLE `historiquetournois`
  ADD CONSTRAINT `historiquetournois_ibfk_1` FOREIGN KEY (`id_membre`) REFERENCES `membre` (`id`),
  ADD CONSTRAINT `historiquetournois_ibfk_2` FOREIGN KEY (`id_tournois`) REFERENCES `tournois` (`id`),
  ADD CONSTRAINT `historiquetournois_ibfk_3` FOREIGN KEY (`id_equipe`) REFERENCES `equipe` (`id`);

--
-- Contraintes pour la table `membre`
--
ALTER TABLE `membre`
  ADD CONSTRAINT `membre_ibfk_1` FOREIGN KEY (`id_equipe`) REFERENCES `equipe` (`id`),
  ADD CONSTRAINT `membre_ibfk_2` FOREIGN KEY (`id_categorie`) REFERENCES `groupe` (`id`);

--
-- Contraintes pour la table `participer`
--
ALTER TABLE `participer`
  ADD CONSTRAINT `participer_ibfk_1` FOREIGN KEY (`id_equipe`) REFERENCES `equipe` (`id`),
  ADD CONSTRAINT `participer_ibfk_2` FOREIGN KEY (`id_tournois`) REFERENCES `tournois` (`id`),
  ADD CONSTRAINT `participer_ibfk_3` FOREIGN KEY (`id_membre`) REFERENCES `membre` (`id`);

--
-- Contraintes pour la table `tournois`
--
ALTER TABLE `tournois`
  ADD CONSTRAINT `tournois_ibfk_1` FOREIGN KEY (`idJeu`) REFERENCES `jeux` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
